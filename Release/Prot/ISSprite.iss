[code]
type
  TImgButtonEvent = procedure(hWnd: HWND; nEventID: Word);
  
const
  SHD_GRADIENTVER     = $0020;
  SHD_GRADIENTHOR     = $0010;

  SHD_STYLEGLOW       = $0080;
  SHD_STYLESHADOW     = $0040;

  btnMouseClickEvent  = 1;
  btnMouseEnterEvent  = 2;
  btnMouseLeaveEvent  = 3;
  btnMouseMoveEvent   = 4;
  btnMouseDownEvent   = 5;
  btnMouseUpEvent     = 6;

  btnAlignDefault     = 1;
  btnAlignTopLeft     = 2;
  btnAlignBottomLeft  = 3;
  btnAlignTopRight    = 4;
  btnAlignBottomRight = 5;
  btnAlignTopCenter   = 6;
  btnAlignBottomCenter= 7;
  btnAlignCenter      = 8;

  BTN_PANEL           = $10;
  BTN_BUTTON          = $20;
  BTN_CHECKBOX        = $40;
  BTN_CHECKBOXEX      = $80;
  
function spImgLoadImage(lpParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'sp7450201@files:ISSprite.dll stdcall';
//������� bmploadImg ��������� BMP-����������� � ������
//AParent         - ����� ����, ��� ����� ���������� �����������
//Filename        - ���� �� ������������ �����������
//ALeft, ATop     - ���������� �������� ������ ���� ������ ����������� (� ����������� ���������� ������� Wnd)
//AWidth, AHeight - ������, ������ �����������
//BColor          - ����, ������� �� ����� ��������� ������ �����������, ���� �� ��������� �������� ������ ���� -1
//Stretch         - �������������� ����������� ��� ���
//IsBkg           - ���� True, �� ������ ����������� �� ���� �����, ����� ��� VCL-�������� ���� TLabel, TPanel � �.�
//                - ���������� �������� ����������� � IsBkg = False
//������������ �������� - ����� �����������, ������������ � ��������� �������� ��� ������ � BMP

function spImgLoadImageFromBuffer(lpParent: HWND; lpBuffer: PAnsiChar; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'sp7450202@files:ISSprite.dll stdcall';
  
procedure spImgRelease(Img: Longword);
  external 'sp7450203@files:ISSprite.dll stdcall';
//������� ����������� �� ������
//Img             - ����� �����������

function spImgCreateFormFromImage(hWnd: HWND; hImage: Longword; nSetTimer: Boolean): Boolean;
  external 'sp7450204@files:IsSprite.dll stdcall';
//������� ����� �� �����������, �� ����� ����� �� ����� ������������ ����������� ��������
//����� ���, ��� ���� ��������� � ������� bmpAddImageFormControl
//����� ����� ����� ����� ����� ������� �������������� � ������� bmpUpdateImageForm
//����������: ������������ ���������� �������� ���� - 8 ��.
//hWnd            - ����� �����
//hImage          - ����� �����������, ������������ ����� bmpLoadImg

procedure spImgUpdateImageForm(hWnd: HWND);
  external 'sp7450205@files:IsSprite.dll stdcall';
//��������� �����, ��������� �� ������ �����������
//hWnd            - ����� �����

procedure spImgSetImageFormAlpha(hWnd: HWND; lpAlpha: Byte);
  external 'sp7450206@files:IsSprite.dll stdcall';
  
procedure spImgGetDimensions(Img: longword; var Width, Height: Integer);
  external 'sp7450207@files:ISSprite.dll stdcall';
//���������� ������������ ������, ������ �����������
//Img             - ����� �����������, ��� �������� ����� �������� ������������ ������ � ������
//Width           - ��������� �� ����������, � ������� ����� ���������� ������ �����������
//Height          - ��������� �� ����������, � ������� ����� ���������� ������ �����������
//                  ��� ������� ���������� ������, ������ ������ �������

procedure spImgSetGifInterval(hImg: Longword; iTime: Longword);
  external 'sp7450208@files:ISSprite.dll stdcall';
  
procedure spImgSetGifIdleTime(hImg: Longword; iTime: Longword);
  external 'sp7450209@files:ISSprite.dll stdcall';
  
procedure spImgSetRotateAngle(hImg: Longword; lpAngle: Integer);
  external 'sp7450210@files:ISSprite.dll stdcall';
  
function spImgGetRotateAngle(hImg: Longword): Integer;
  external 'sp7450211@files:ISSprite.dll stdcall';
  
procedure spImgSetBackgroundColor(Img: longword; aColor: integer);
  external 'sp7450212@files:ISSprite.dll stdcall';
//������������� "���������� ����" ��� �����������
//Img             - ����� �����������
//aColor          - ����, ������� �� ����� ��������� ������ �����������, ���� �� ��������� �������� ������ ���� -1

procedure spImgSetTransparent(Img: Longword; aAlpha: Integer);
  external 'sp7450213@files:ISSprite.dll stdcall';
//������� ��������� ������� ������������ ��� �����������
//Img             - ����� �����������
//aAlpha          - ������ ������������ ���������� �������� 0 - 255
//                  0 - ��������� ����������, 255 - ��������� ������������

function spImgGetTransparent(Img: Longword): Integer;
  external 'sp7450214@files:ISSprite.dll stdcall';
//���������� ������� ������������ �����������
//Img             - ����� �����������
//������������ �������� �� 0 �� 255

function spImgGetParent(Img: Longword): HWND;
  external 'sp7450215@files:ISSprite.dll stdcall';
//���������� ������� �������� �����������
//Img             - ����� �����������, �������� �������� ����� ��������
//                  ���� ����������� �� ������� ���������� INVALID_HANDLE_VALUE

procedure spImgSetVisibility(Img: longword; Visible: Boolean);
  external 'sp7450216@files:ISSprite.dll stdcall';
//������������� �������� ��������� �����������
//Img             - ����� �����������, ��� �������� ����� ���������� �������� ���������
//Visible         - �������� ������� ����� ���������

function spImgGetVisibility(Img: longword): Boolean;
  external 'sp7450217@files:ISSprite.dll stdcall';
//���������� �������� ��������� �����������
//Img             - ����� �����������, ��� �������� ����� �������� �������� ���������

procedure spImgSetPos(Img: Longword; ALeft, ATop, AWidth, AHeight: Integer);
  external 'sp7450218@files:ISSprite.dll stdcall';
//��������� ��������� ����������� �� �����
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �����������

procedure spImgGetPos(Img: Longword; var ALeft, ATop, AWidth, AHeight: Integer);
  external 'sp7450219@files:ISSprite.dll stdcall';
//���������� ��������� ����������� �� �����
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �����������

procedure spImgSetVisiblePart(Img: Longword; ALeft, ATop, AWidth, AHeight: Integer);
  external 'sp7450220@files:ISSprite.dll stdcall';
//������������� ������� ����� ������ �����������
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �������� �����
//                  ���� Stretch=False �� ������������, ��������� ����� ����������� � ������� � ������� ������
//                  ������ � ������ ������������� �������� bmpSetPos

procedure spImgGetVisiblePart(Img: Longword; var ALeft, ATop, AWidth, AHeight: Integer);
  external 'sp7450221@files:ISSprite.dll stdcall';
//���������� ������� ����� ������ �����������
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �������� �����

procedure spImgSetVisiblePie(Img: Longword; lpVisAngle: Integer);  //!! not working yet !!
  external 'sp7450222@files:ISSprite.dll stdcall';
  
procedure spImgSetSpriteCount(Img: Longword; SpriteCount: Integer);
  external 'sp7450223@files:ISSprite.dll stdcall';
//��� ������ ������������� ����, ��� ����������� �������� ��������, ������������� ������������ ������ �������
//Img             - ����� �����������, ��� �������� ����� ���������� ������������ ������ �������
//SpriteCount     - ����� �������� � �����, �� �� ������������ ������
//                  ������� �������� ��� ��� ������������ �������� ����������� ����� ������������ �����������
//                  ����� ������� �������� ��, ��� ������ ������ ���� �����������, �.� ��� ����������� ������
//                  ������������� � �������

function spImgGetSpriteCount(Img: Longword): Integer;
  external 'sp7450224@files:ISSprite.dll stdcall';
//���������� ������� ������ �������
//Img             - ����� �����������, ��� �������� ����� ������ ������ �������

procedure spImgSetSpriteIndex(Img: Longword; Index: Integer);
  external 'sp7450225@files:ISSprite.dll stdcall';
//������������� ������ �������
//Img             - ����� �����������, ��� �������� ����� ���������� ������ �������
//Index           - ���������� ��� ������, ��������� �������� �� 1 �� ������������� �������
//                  ��� ������ �� ������� ���������� �������� ������������� ������������� �� ���������� ��������

function spImgGetSpriteIndex(Img: Longword): Integer;
  external 'sp7450226@files:ISSprite.dll stdcall';
//���������� ������������ ������ �������, �������� ������ ����� ������ bmpSetProperties
//Img             - ����� �����������, ��� �������� ����� ������ ������������ ������

procedure spApplyChanges(h: HWND);
  external 'sp7450101@files:ISSprite.dll stdcall';
//��������� �������� ����������� �� �����
//h               - ����� �����, �� ������� ���������� ������������ �����������

procedure spInitialize(nUseLogicPixels: Boolean; nUseSmartPaint: Boolean);
  external 'sp7450102@files:ISSprite.dll stdcall';
//����������� �������� ����� �������������� ����������

procedure spShutdown();
  external 'sp7450103@files:ISSprite.dll stdcall';
//����������� �������� ��� ���������� ����������

function spShdAddText(lpParent: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpCaption: PAnsiChar; lpTextColor: Integer; aFont: Longword): Longword;
  external 'sp7450301@files:ISSprite.dll stdcall';
  
procedure spShdSetText(hText: Longword; lpCaption: PAnsiChar);
  external 'sp7450302@files:ISSprite.dll stdcall';

procedure spShdSetShadow(hText: Longword; lpShadowColor: Cardinal; lpShadowSize, lpShadowIntense: Byte; lpShadowStyle: DWORD);
  external 'sp7450303@files:ISSprite.dll stdcall';

procedure spShdSetRotateAngle(hText: Longword; lpAngle: Integer);
  external 'sp7450304@files:ISSprite.dll stdcall';
  
procedure spShdSetVisible(hText: Longword; lpVisible: Boolean);
  external 'sp7450305@files:ISSprite.dll stdcall';
  
function spShdGetVisible(hText: Longword): Boolean;
  external 'sp7450306@files:ISSprite.dll stdcall';

procedure spShdAddGradient(hText: Longword; lpBeginColor, lpEndColor: Cardinal; lpStyle: DWORD);
  external 'sp7450307@files:ISSprite.dll stdcall';
  
procedure spShdFreeGradient(hText: Longword);
  external 'sp7450308@files:ISSprite.dll stdcall';

procedure spShdSetTextColor(hText: Longword; lpTextColor: Cardinal);
  external 'sp7450309@files:ISSprite.dll stdcall';
  
procedure spShdSetShadowColor(hText: Longword; lpShadowColor: Cardinal);
  external 'sp7450310@files:ISSprite.dll stdcall';
  
procedure spShdSetCharacterExtra(hText: Longword; lpCharExtra: Integer);
  external 'sp7450311@files:ISSprite.dll stdcall';
  
procedure spShdSetPos(hText: Longword; lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'sp7450312@files:ISSprite.dll stdcall';

procedure spShdGetPos(hText: Longword; var lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'sp7450313@files:ISSprite.dll stdcall';

//Fonts
function spFntCreateFont(lpFontName: PAnsiChar; lpBold, lpItalic, lpUnderline, lpStrikeOut: Boolean; lpFontSize: Integer): Longword;
  external 'sp7450501@files:ISSprite.dll stdcall';

function spFntCreateFont2(lpFontName: PAnsiChar; lpWBold: Integer; lpItalic, lpUnderline, lpStrikeOut: Boolean; lpFontSize: Integer): Longword;
  external 'sp7450502@files:ISSprite.dll stdcall';
  
function spFntFreeFont(lpFont: Longword): Boolean;
  external 'sp7450503@files:ISSprite.dll stdcall';

function spFntRegisterFont(lpFilename: PAnsiChar): Boolean;
  external 'sp7450504@files:ISSprite.dll stdcall';

function spFntUnregisterFont(lpFilename: PAnsiChar): Boolean;
  external 'sp7450505@files:ISSprite.dll stdcall';
//Buttons
function WrapButtonProc(callback: TImgButtonEvent; Paramcount: Integer): Longword;
  external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';

function spBtnCreateButton(hParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'sp7450401@files:ISSprite.dll stdcall';
  
function spBtnCreateButtonFromBuffer(hParent: HWND; lpBuffer: PAnsiChar; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'sp7450402@files:ISSprite.dll stdcall';
  
procedure spBtnRefresh(hBtn: HWND);
  external 'sp7450403@files:ISSprite.dll stdcall';

procedure spBtnSetCursor(hBtn: HWND; lpCursor: Longword);
  external 'sp7450404@files:ISSprite.dll stdcall';

function spBtnGetSysCursor(lpCursorID: Integer): Longword;
  external 'sp7450405@files:ISSprite.dll stdcall';

procedure spBtnSetFont(hBtn: HWND; lpFont: Longword);
  external 'sp7450406@files:ISSprite.dll stdcall';
  
procedure spBtnSetFontColor(hBtn: HWND; lpColorDefault, lpColorHover, lpColorPressed, lpColorDisabled: Cardinal);
  external 'sp7450407@files:ISSprite.dll stdcall';

procedure spBtnSetText(hBtn: HWND; lpText: PAnsiChar);
  external 'sp7450408@files:ISSprite.dll stdcall';

procedure spBtnSetTextAlignment(hBtn: HWND; lpAlign: DWORD);
  external 'sp7450409@files:ISSprite.dll stdcall';

procedure spBtnSetTextIndent(hBtn: HWND; iLeft, iTop: Integer);
  external 'sp7450410@files:ISSprite.dll stdcall';
  
procedure spBtnSetEvent(hBtn: HWND; lpEventID: Integer; lpEvent: Longword);
  external 'sp7450411@files:ISSprite.dll stdcall';

procedure spBtnSetPos(hBtn: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'sp7450412@files:ISSprite.dll stdcall';

procedure spBtnGetPos(hBtn: HWND; var lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'sp7450413@files:ISSprite.dll stdcall';

procedure spBtnSetEnabled(hBtn: HWND; lpEnabled: Boolean);
  external 'sp7450414@files:ISSprite.dll stdcall';

function spBtnGetEnabled(hBtn: HWND): Boolean;
  external 'sp7450415@files:ISSprite.dll stdcall';

procedure spBtnSetVisible(hBtn: HWND; lpVisible: Boolean);
  external 'sp7450416@files:ISSprite.dll stdcall';

function spBtnGetVisible(hBtn: HWND): Boolean;
  external 'sp7450417@files:ISSprite.dll stdcall';

procedure spBtnSetChecked(hBtn: HWND; lpChecked: Byte);
  external 'sp7450418@files:ISSprite.dll stdcall';

function spBtnGetChecked(hBtn: HWND): Byte;
  external 'sp7450419@files:ISSprite.dll stdcall';

procedure spBtnSetTransparent(hBtn: HWND; lpAlpha: Byte);
  external 'sp7450420@files:ISSprite.dll stdcall';

function spBtnGetTransparent(hBtn: HWND): Byte;
  external 'sp7450421@files:ISSprite.dll stdcall';

procedure MoveSpriteHor(Img: Longword; Step, MinLeft, MaxLeft: Integer);
var
  x, y, x2, y2, ox, oy, px, py, px2, py2: Integer;
begin
  spImgGetPos(Img, x, y, x2, y2);
  spImgGetDimensions(Img, ox, oy);
  spImgGetVisiblePart(Img, px, py, px2, py2);
  x:=x+Step;
  if x2=0 then if Step<0 then x:= MaxLeft else x:= MinLeft;

  if Step>0 then begin
    if ((x+x2)>MaxLeft)and(x2>0) then begin
      px2:=px2-Step;
    end;
    if (x=MinLeft)and(x2<ox) then begin
      px2:=px2+Step;
      px:=ox-px2;
      x:=x-Step
    end;
  end else begin
    if (x<MinLeft)and(x2>0) then begin
      px2:=px2+Step;
      px:=ox-px2;
      x:=x-Step;
    end;
    if (x+ox>=MaxLeft)and(x2<ox) then begin
      px:=0;
      px2:=px2-Step;
    end;
  end;
  x2:=px2;
  spImgSetVisiblepart(Img, px, py, px2, py2);
  spImgSetPos(Img, x, y, x2, y2);
end;

function IsButtonChecked(hBtn: HWND): Boolean;
begin
  Result:= (spBtnGetChecked(hBtn) > 0);
end;

function ImgLoadFromBuffer(lpParent: HWND; lpFilename: String; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
var
  buffer: AnsiString;
  FileSize: Longint;
begin
  lpFilename:= ExtractFileName(lpFilename);
  FileSize:= ExtractTemporaryFileSize(lpFilename);
  SetLength(Buffer, FileSize);
  #ifdef UNICODE
    ExtractTemporaryFileToBuffer(lpFilename, CastAnsiStringToInteger(Buffer));
  #else
    ExtractTemporaryFileToBuffer(lpFilename, CastStringToInteger(Buffer));
  #endif
  Result:= spImgLoadImageFromBuffer(lpParent, PAnsiChar(buffer), FileSize, lpLeft, lpTop, lpWidth, lpHeight, lpStretch, lpIsBkg);
  SetLength(buffer, 0);
end;

function BtnLoadFromBuffer(hParent: HWND; lpFilename: String; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
var
  buffer: AnsiString;
  FileSize: Longint;
begin
  lpFilename:= ExtractFileName(lpFilename);
  FileSize:= ExtractTemporaryFileSize(lpFilename);
  SetLength(Buffer, FileSize);
  #ifdef UNICODE
    ExtractTemporaryFileToBuffer(lpFilename, CastAnsiStringToInteger(Buffer));
  #else
    ExtractTemporaryFileToBuffer(lpFilename, CastStringToInteger(Buffer));
  #endif
  Result:= spBtnCreateButtonFromBuffer(hParent, PAnsiChar(buffer), FileSize, lpLeft, lpTop, lpWidth, lpHeight, lpStyle, lpShadowSize);
  SetLength(buffer, 0);
end;