library ISSprite;

{$R Copyright.res}

//{$define DEBUG}
//{$define RELEASE}
//{$define ICM}

uses
	Windows;

{$DebugInfo Off}
{$Align 4}
{$LocalSymbols Off}
{$DefinitionInfo Off}
{$ReferenceInfo Off}

type
  ISSpriteMainInfo = packed record
		InfoPoint      : Pointer;
		InfoCount      : Longint;
    hProcHeap      : Cardinal;
    LPixelsY       : Longint;
    DisplaySize    : TSize;
    spNeedLPY      : Boolean;
    ISSpriteInit   : Boolean;
    SPUpdating     : Boolean;
    SPNeedCtlColor : Boolean;
    SPDebugDraw    : Boolean;
  end;

var
	ISSpriteMiniature: Boolean;	
  ISSpriteMain: ISSpriteMainInfo;

{$ifdef DEBUG}
  {$I 'ISSpriteDebugFunc.pas'}
{$endif}
{$I 'ISSpriteMemManager.pas'}
{$I 'ISSpriteOle32.pas'}
{$I 'ISSpriteGDIAPI.pas'}
{$I 'ISSpriteGDIPlusAPI.pas'}
{$I 'ISSpriteAsmString.pas'}
{$I 'ISSpriteTextUtils.pas'}
{$I 'ISSpriteHeader.pas'}
{$I 'ISSpriteShadowText.pas'}
{$I 'ISSpriteImgButton.pas'}
{$I 'ISSpriteFont.pas'}
//{$I 'ISSpriteWinPlus.pas'} {my_add}

function spImgAddSprite(lpParent: HWND; lpBitmap: GPBITMAP; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
var
	FI		: PFormInfo;
	SP		: PSprite;
  x, y	: Cardinal;
  n, k	: Longword;
begin
  Result:= Longword(nil);

  FI:= spGetFormInfo(lpParent);
	
  if FI = nil then FI:= spAddFormInfo(lpParent);
  if (FI = nil) or (FI^.lpSpritesCount = 0) then Exit;

  Dec(FI^.lpSpritesCount);
  SP:= PSprite(Longword(FI^.lpSprites) + Longword(FI^.lpSpritesCount*SizeOf(TSprite)));

  FillChar(SP^, SizeOf(TSprite), 0);
	with PSprite(SP)^ do begin
		spLeft        := lpLeft;
		spTop         := lpTop;
		spWidth       := lpWidth;
		spHeight      := lpHeight;
    spCutColor    := -1;
    spMyImage     := lpBitmap;

    GdipGetImageWidth(spMyImage, x);
    GdipGetImageHeight(spMyImage, y);
		
		{my_add}
		if not lpStretch then begin
			if spWidth = 0 then spWidth:= x; 
			if spHeight = 0 then spHeight:= y;
		end; {end}
		
		spImgWidth    := x;
		spImgHeight   := y;
    splwaAlpha    := 255;
		spParent      := lpParent;
    spParentFI    := FI;
		spShowImg     := True;
    spVisiblePie  := -1;

    Move((@spImgWidth)^, (@spMaxWidth)^, 8);
    Move(SP^, (@spOldLeft)^, 16);
    Move((@spWidth)^, (@spPartWidth)^, 8);

		if lpIsBkg then begin
      spFlags:= SP_BKDRAW;
      Inc(FI^.lpSpritesBkCount);
    end else begin
      spFlags:= SP_DRAW;
      Inc(FI^.lpSpritesFrCount);
    end;

		if lpStretch then begin
			spFlags     := spFlags or SP_STRETCH;
			spPartWidth := spImgWidth;
			spPartHeight:= spImgHeight;
		end;
		
    spFlags       := spFlags or SP_REPAINT;
    spGifTimer    := Longword(SP);
    spGifIdleTime := 500;

    GdipCreatePath(FillModeAlternate, spPiePath);
    GdipCreateImageAttributes(spImageAttribs);
    GdipImageGetFrameDimensionsCount(spMyImage, n);
    spGifGUID:= HeapAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, SizeOf(TGUID)*n);
    GdipImageGetFrameDimensionsList(spMyImage, spGifGUID, n);
    GdipImageGetFrameCount(spMyImage, spGifGUID, k);
    spGifCurFrame:= 0;
    if k > 1 then begin
      spGifIdleTime:= k; {my_add}
      spGifMaxFrames:= k;
      spFlags:= (spFlags and not (SP_BKDRAW or SP_DRAW)) or SP_GIFDRAW;
      SetTimer(spParent, spGifTimer, 75, @spGifTimerProc);
    end;

		Result:= spGifTimer;
	end;
end;

{$ifdef RELEASE}
function sp101(nUseLogicPixels: Boolean; nUseSmartPaint: Boolean): Boolean; stdcall;
{$else}
function spInitialize(nUseLogicPixels: Boolean; nUseSmartPaint: Boolean): Boolean; stdcall;
{$endif}
var
  ImgBtnClass: TWndClassEx;
	WizardForm: HWND; {my_add}
  DC: HDC;
begin
	{my_add miniature}
	WizardForm:= FindWindow('TWizardForm', nil); 
	if (WizardForm > 0) and not ISSpriteMiniature then begin
		if GetWindowLong(GetWindow(WizardForm, GW_OWNER), GWL_HWNDPARENT) = 0 then ISSpriteMiniature:= True
		else begin 
			SetWindowLong(WizardForm, GWL_HWNDPARENT, GetWindowLong(GetWindow(WizardForm, GW_OWNER), GWL_HWNDPARENT));
			ISSpriteMiniature:= True;
		end;
	end;
	{end}
	
	Result:= False;
  if ISSpriteMain.ISSpriteInit then Exit;
	
  {ShowMessage('TSprite: '+IntToStr(SizeOf(TSprite))+' | '+IntToStr(SizeOf(TSprite)*MAX_SPRITES)+#13#10+
              'TShadowText: '+IntToStr(SizeOf(TShadowText))+' | '+IntToStr(SizeOf(TShadowText)*MAX_TEXTS)+#13#10+
              'TImgButton: '+IntToStr(SizeOf(TImgButton))+' | '+IntToStr(SizeOf(TImgButton)*MAX_BUTTONS)+#13#10+
              'TImageForm: '+IntToStr(SizeOf(TImageForm))+#13#10+
              'TFormInfo: '+IntToStr(SizeOf(TFormInfo))+' | '+IntToStr(SizeOf(TFormInfo)*MAX_INFO));}

  GDIInitialize();
  ISSpriteMain.hProcHeap:= GetProcessHeap();

  ISSpriteMain.InfoPoint:= VirtualAlloc(nil, (SizeOf(TFormInfo)*MAX_INFO)+MEM_MAX_COUNT*SizeOf(TMemoryManager), MEM_RESERVE or MEM_COMMIT, PAGE_READWRITE);
  PMemTable:= Pointer(Longword(ISSpriteMain.InfoPoint)+SizeOf(TFormInfo)*MAX_INFO);

  FillChar(ImgBtnClass, SizeOf(TWndClassEx), 0);
  ImgBtnClass.cbSize         := SizeOf(TWndClassEx);
  ImgBtnClass.hInstance      := hInstance;
  ImgBtnClass.lpfnWndProc    := @spBtnWndProc;
  ImgBtnClass.lpszClassName  := btnClassName;
  RegisterClassEx(ImgBtnClass);

  DC:= GetDC(0);
  ISSpriteMain.LPixelsY:= GetDeviceCaps(DC, LOGPIXELSY);
  ISSpriteMain.DisplaySize.cx:= GetDeviceCaps(DC, HORZRES);
  ISSpriteMain.DisplaySize.cy:= GetDeviceCaps(DC, VERTRES);
  ReleaseDC(0, DC);

  CRC32Init;

  ISSpriteMain.spNeedLPY     := nUseLogicPixels;
  ISSpriteMain.SPNeedCtlColor:= nUseSmartPaint;
  ISSpriteMain.SPUpdating    := False;
  ISSpriteMain.ISSpriteInit  := True;
	
	Result:=ISSpriteMain.ISSpriteInit;
end;

{$ifdef RELEASE}
procedure sp102; stdcall;
{$else}
procedure spShutdown; stdcall;
{$endif}
var
  i, n: Smallint;
  FI  : PFormInfo;
  SP  : PSprite;
  ST  : PShadowText;
  IB  : PImgButton;
  MM  : PMemManager;
begin
  if not ISSpriteMain.ISSpriteInit then Exit;

  n:= ISSpriteMain.InfoCount;
  if n <> MAX_INFO then
  repeat
    FI:= PFormInfo(Longint(ISSpriteMain.InfoPoint)+n*SizeOf(TFormInfo));

    i:= FI^.lpSpritesCount;
    if i <> MAX_SPRITES then
    repeat
      SP:= PSprite(Longint(FI^.lpSprites)+i*SizeOf(TSprite));
      GdipDeletePath(SP^.spPiePath);
      GdipDisposeImageAttributes(SP^.spImageAttribs);
      KillTimer(SP^.spParent, SP^.spGifTimer);
      Inc(i);
    until i=MAX_SPRITES;

    i:= FI^.lpTextsCount;
    if i <> MAX_TEXTS then
    repeat
      ST:= PShadowText(Longint(FI^.lpTexts)+i*SizeOf(TShadowText));
      GdipDisposeImage(ST^.shdImage);
      SelectObject(ST^.shdTextDC, ST^.shdOldBitmap);
      DeleteObject(ST^.shdTextDC);
      DeleteObject(ST^.shdTextBitmap);
      DeleteObject(ST^.shdFont);
      HeapFree(ISSpriteMain.hProcHeap, 0, ST^.shdCaption);
      VirtualFree(ST^.shdCbSpace, 0, MEM_RELEASE);
      Inc(i);
    until i=MAX_TEXTS;

    i:= FI^.lpButtonsCount;
    if i <> MAX_BUTTONS then
    repeat
      IB:= PImgButton(Longint(FI^.lpButtons)+i*SizeOf(TImgButton));
      DestroyWindow(IB.btnHandle);
      SelectObject(IB^.btnCacheDC, IB^.btnCacheOld);
      DeleteObject(IB^.btnCacheDC);
      DeleteObject(IB^.btnCacheBmp);
      SelectObject(IB^.btnSurface, IB^.btnSurfaceOld);
      DeleteObject(IB^.btnSurface);
      DeleteObject(IB^.btnSurfaceBmp);
      VirtualFree(IB^.btnCbSpace, 0, MEM_RELEASE);  
			
      if IB^.btnText <> nil then HeapFree(ISSpriteMain.hProcHeap, 0, IB^.btnText);
      if IB^.btnTag <> nil then HeapFree(ISSpriteMain.hProcHeap, 0, IB^.btnTag);

      Inc(i);
    until i = MAX_BUTTONS;

    if FI^.lpTimer<>INVALID_HANDLE_VALUE then KillTimer(FI^.lpHandle, SP_PAINTTIMER);
    if FI^.lpOldProc <> 0 then SetWindowLong(FI^.lpHandle, GWL_WNDPROC, FI^.lpOldProc);

    DeleteObject(FI^.lpCtlBrush);
    GdipDeleteGraphics(FI^.lpSurfaceGP);
    SelectObject(FI^.lpSurface, FI^.lpSurfaceOld);
    DeleteObject(FI^.lpSurface);
    DeleteObject(FI^.lpSurfaceBmp);

    GdipDeleteGraphics(FI^.lpLayerGP);
    SelectObject(FI^.lpLayer, FI^.lpLayerOld);
    DeleteObject(FI^.lpLayer);
    DeleteObject(FI^.lpLayerBmp);

    GdipDeleteGraphics(FI^.lpBkLayerGP);
    SelectObject(FI^.lpBkLayer, FI^.lpBkLayerOld);
    DeleteObject(FI^.lpBkLayer);
    DeleteObject(FI^.lpBkLayerBmp);

    VirtualFree(FI^.lpRawData, 0, MEM_RELEASE);
    Inc(n);
  until n = MAX_INFO;

  i:= MemImgCount;
  if i <> MEM_MAX_COUNT then
  repeat
    MM:= PMemManager(Longint(PMemTable)+i*SizeOf(TMemManager));
    GdipDisposeImage(MM^.PData);
    Inc(i);
  until i = MEM_MAX_COUNT;

  VirtualFree(ISSpriteMain.InfoPoint, 0, MEM_RELEASE);
  UnregisterClass(btnClassName, hInstance);
  GDIShutDown();
  ISSpriteMain.ISSpriteInit:= False;
end;

{$ifdef RELEASE}
function sp201(hWnd: HWND; nSetTimer: Integer): Boolean; stdcall;
{$else}
function spImageFormCreate(hWnd: HWND; nSetTimer: Integer): Boolean; stdcall;
{$endif}
var
  FI: PFormInfo;
	rc: TRect; {my_add}
begin
  Result:= False;
  FI:= spGetFormInfo(hWnd);
  if FI = nil then Exit;
	
	{my_add set form styles}
	GetWindowRect(hWnd, rc); 
	SetWindowLong(hWnd, GWL_STYLE, GetWindowLong(hWnd, GWL_STYLE) and not WS_BORDER and not WS_SIZEBOX and not WS_DLGFRAME);
	with rc do SetWindowPos(hWnd, HWND_TOP, Left, Top, Right - Left, Bottom - Top, SWP_FRAMECHANGED);
	{end}
	
  FI^.lpOldStyle:= SetWindowLong(hWnd, GWL_EXSTYLE, Longint(GetWindowLong(hWnd, GWL_EXSTYLE) or WS_EX_LAYERED));
  FI^.lpIsLayered:= True;
  if nSetTimer > 0 then FI^.lpTimer:= SetTimer(hWnd, SP_PAINTTIMER, nSetTimer, nil);
	
  Result:= True;
end;

{$ifdef RELEASE}
procedure sp202(hWnd: HWND; lpAlpha: Byte); stdcall;
{$else}
procedure spImageFormSetAlpha(hWnd: HWND; lpAlpha: Byte); stdcall;
{$endif}
var
  FI: PFormInfo;
begin
  FI:= spGetFormInfo(hWnd);
  if FI <> nil then begin
    FI^.lplwaAlpha:= lpAlpha;
    FI^.lpBlendOp:= (1 shl 24) or (lpAlpha shl 16);
  end;
end;

{$ifdef RELEASE}
procedure sp203(hWnd: HWND); stdcall;
{$else}
procedure spImageFormUpdate(hWnd: HWND); stdcall;
{$endif}
var
  FI: PFormInfo;
begin
  FI:= spGetFormInfo(hWnd);
  if (FI = nil) or (not FI^.lpIsLayered) then Exit;

  spUpdateLayeredWindow(hWnd, FI);
end;

{$ifdef RELEASE}
procedure sp204(hWnd: HWND); stdcall;
{$else}
procedure spApplyChanges(hWnd: HWND); stdcall;
{$endif}
var
  n1, n2  : Integer;
  i       : Integer;
	r, r1   : TRect;
  ST      : PShadowText;
  FI      : PFormInfo;
  IB      : PImgButton;
  SP      : PSprite;
  Color   : TColorMatrix;
//qpc1, qpc2: TLargeInteger;
begin
//QueryPerformanceCounter(qpc1);
  FI:= spGetFormInfo(hWnd);
  if (FI = nil) or (ISSpriteMain.SPUpdating) then Exit;

  ISSpriteMain.SPUpdating := True;
  n1      := 0;  n2       := 0;
  r.Left  := 0;  r.Top    := 0;
  r.Right := 0;  r.Bottom := 0;

  i:= MAX_SPRITES-1;
  SP:= PSprite(Longword(FI^.lpSprites) + Longword(i*SizeOf(TSprite)));
  repeat
		with SP^ do begin
			if (spFlags and SP_NEEDPAINT <> 0) then begin
        GdipResetImageAttributes(spImageAttribs, ColorAdjustTypeBitmap);
        if spCutColor <> -1 then
          GdipSetImageAttributesColorKeys(spImageAttribs, ColorAdjustTypeBitmap, true, spCutColor, spCutColor);
        if splwaAlpha <> $FF then begin
          Move(Gray, Color, SizeOf(TColorMatrix));
          Color[3, 3]:= splwaAlpha/$FF;
          GdipSetImageAttributesColorMatrix(spImageAttribs, ColorAdjustTypeBitmap, true, @Color, @Gray, ColorMatrixFlagsDefault);
        end;
        if spVisiblePie <> -1 then begin
          GdipResetPath(spPiePath);
          GdipAddPathPieI(spPiePath, spLeft, spTop, spWidth, spHeight, -90, spVisiblePie);
        end;
        if (spFlags and SP_BKDRAW <> 0) then
          Inc(n1)
        else if (spFlags and SP_DRAW <> 0) then
          Inc(n2);
        if (not FI^.lpIsLayered) then begin
          if spRotate<>spOldRotate then begin
            if (spFlags and SP_IMGMOVED <> 0) then begin
              spSetRotateRect(@r1, @spOldLeft, spOldRotate);
              if (not IsRectEmpty(r1)) then UnionRect(r, r, r1);
            end;
            spSetRotateRect(@r1, @spLeft, spOldRotate);
            if (not IsRectEmpty(r1)) then UnionRect(r, r, r1);
          end;
          if (spFlags and SP_IMGMOVED <> 0) then begin
            spSetRotateRect(@r1, @spOldLeft, spRotate);
            if (not IsRectEmpty(r1)) then UnionRect(r, r, r1);
          end;
          spSetRotateRect(@r1, @spLeft, spRotate);
          if (not IsRectEmpty(r1)) then UnionRect(r, r, r1);
        end;
				spFlags:= spFlags and not (SP_NEEDPAINT or SP_IMGMOVED);
			end;
		end;
    Dec(i);
    Dec(SP);
  until i < FI^.lpSpritesCount;

  i:= MAX_BUTTONS-1;
  IB:= PImgButton(Longword(FI^.lpButtons) + Longword(i*SizeOf(TImgButton)));
  repeat
    if IB^.btnRebuild then begin
      BuildButton(IB);
      IB^.btnRebuild:= False;
    end;
    if IB^.btnVisible then
      InvalidateRect(IB^.btnHandle, nil, False);
    Dec(i);
    Dec(IB);
  until i<FI^.lpButtonsCount;

  i:= MAX_TEXTS-1;
  ST:= PShadowText(Longword(FI^.lpTexts) + Longword(i*SizeOf(TShadowText)));
  repeat
    with ST^ do begin
      if (shdFlags and SHD_NEEDBUILD <> 0) then begin
        BuildText(ST);
        shdFlags:= shdFlags and not SHD_NEEDBUILD;
      end;
      if (shdFlags and SHD_NEEDPAINT <> 0)and(not FI^.lpIsLayered) then begin
        SetRectI(@r1, @shdOldLeft);
        if (not IsRectEmpty(r1)) then UnionRect(r, r, r1);
        SetRectI(@r1, @shdLeft);
        if (not IsRectEmpty(r1)) then UnionRect(r, r, r1);
        shdFlags:= shdFlags and not (SHD_NEEDPAINT);
      end;
    end;
    Dec(i);
    Dec(ST);
  until i<FI^.lpTextsCount;

  if n1 > 0 then ClearBits(FI^.lpBkLayerBits, FI^.lpBitsCount);
  if n2 > 0 then ClearBits(FI^.lpLayerBits, FI^.lpBitsCount);

  i:= MAX_SPRITES-1;
  SP:= PSprite(Longword(FI^.lpSprites) + Longword(i*SizeOf(TSprite)));
  repeat
    if SP^.spShowImg then begin
      if (SP^.spFlags and SP_BKDRAW <> 0) and (n1 > 0) then spDrawImage(FI^.lpBkLayerGP, SP)
      else if (SP^.spFlags and SP_DRAW <> 0) and (n2 > 0) then spDrawImage(FI^.lpLayerGP, SP);
    end;
    Dec(i);
    Dec(SP);
  until i<FI^.lpSpritesCount;

  if (not IsRectEmpty(r)) and (not FI.lpIsLayered) then begin
    InvalidateRect(hWnd, @r, false);
    UpdateWindow(hWnd);
  end;
  ISSpriteMain.SPUpdating:= False;
//QueryPerformanceCounter(qpc2);
//ShowMessage('spApplyChanges time: '+intToStr(qpc2-qpc1));
end;

{$ifdef RELEASE}
function sp501(lpParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$else}
function spImgLoadImage(lpParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$endif}
var
  TmpImg: GPBITMAP;
begin
  Result:= Longword(nil);
  TmpImg:= GDILoadFromFile(lpFilename);
  if TmpImg <> nil then Result:= spImgAddSprite(lpParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStretch, lpIsBkg);
end;

{$ifdef RELEASE}
function sp502(lpParent: HWND; lpBuffer: Pointer; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$else}
function spImgLoadImageFromBuffer(lpParent: HWND; lpBuffer: Pointer; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$endif}
var
  TmpImg: GPBITMAP;
begin
  Result:= Longword(nil);
  TmpImg:= GDILoadFromBufer(lpBuffer, lpBuffSize);
  if TmpImg <> nil then Result:= spImgAddSprite(lpParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStretch, lpIsBkg);
end;

{$ifdef RELEASE}
function sp503(lpParent: HWND; lpInstance: Longword; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$else}
function spImgLoadImageFromResourceName(lpParent: HWND; lpInstance: Longword; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$endif}
var
	hRes, hResLoad: Longword;
  ResSize: Cardinal;
  TmpImg: GPBITMAP;
  pRes: Pointer;
begin
  Result:= Longword(nil);

  if lpInstance <> 0 then begin
    hRes := FindResource(lpInstance, lpResName, ResourceSwitch(lpResType));

    if hRes <> 0 then begin
      hResLoad:= LoadResource(lpInstance, hRes);
      if hResLoad <> 0 then begin
        ResSize:= SizeOfResource(lpInstance, hRes);
        pRes:= LockResource(hResLoad);

        TmpImg:= GDILoadFromBufer(pRes, ResSize);
        if TmpImg <> nil then
          Result:= spImgAddSprite(lpParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStretch, lpIsBkg);
      end;
    end;
  end;
end;

{$ifdef RELEASE}
function sp504(lpParent: HWND; lpResLibrary: PAnsiChar; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$else}
function spImgLoadImageFromResourceLibrary(lpParent: HWND; lpResLibrary: PAnsiChar; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword; stdcall;
{$endif}
var
	hLib, hRes, hResLoad: Longword;
  ResSize: Cardinal;
  TmpImg: GPBITMAP;
  pRes: Pointer;
begin
  Result:= Longword(nil);

  hLib:= LoadLibraryA(lpResLibrary);

  if hLib <> 0 then begin
    hRes:= FindResourceA(hLib, lpResName, ResourceSwitch(lpResType));

    if hRes <> 0 then begin
      hResLoad:= LoadResource(hLib, hRes);

      if hResLoad <> 0 then begin
        ResSize:= SizeOfResource(hLib, hRes);
        pRes:= LockResource(hResLoad);

        TmpImg:= GDILoadFromBufer(pRes, ResSize);
        if TmpImg <> nil then
          Result:= spImgAddSprite(lpParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStretch, lpIsBkg);
      end;
    end;
    FreeLibrary(hLib);
  end;
end;

{$ifdef RELEASE}
procedure sp505(Img: Longword); stdcall;
{$else}
procedure spImgRelease(Img: Longword); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(Img);
  if SP = nil then Exit;
	
	SP^.spFlags:= SP_NEEDPAINT;
end;

{$ifdef RELEASE}
procedure sp506(hImg: Longword); stdcall;
{$else}
procedure spImgBringToFront(hImg: Longword); stdcall;
{$endif}
var
  SP, cur, next: PSprite;
  FI: PFormInfo;
  i, k, idx: Longint;
  tmp: TSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  FI:= SP^.spParentFI;
  idx:= Longword(Longword(SP) - Longword(FI^.lpSprites)) div SizeOf(TSprite);

  i:= idx;
  k:= idx - 1;

  cur:= PSprite(Longword(FI^.lpSprites) + Longword(i*SizeOf(TSprite)));
  next:= PSprite(Longword(FI^.lpSprites) + Longword(k*SizeOf(TSprite)));

  Move(SP^, tmp, SizeOf(TSprite));
  while (k >= FI^.lpSpritesCount) do begin
    move(next^, cur^, SizeOf(TSprite));
    cur^.spFlags:= cur^.spFlags or SP_REPAINT;

    Dec(i);
    Dec(k);
    Dec(cur);
    Dec(next);
  end;

  if i <> idx then begin
    Move(tmp, cur^, SizeOf(TSprite));
    cur^.spFlags:= cur^.spFlags or SP_REPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp507(hImg: Longword); stdcall;
{$else}
procedure spImgSendToBack(hImg: Longword); stdcall;
{$endif}
var
  SP, cur, next: PSprite;
  FI: PFormInfo;
  i, k, idx: Longword;
  tmp: TSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  FI:= SP^.spParentFI;
  idx:= Longint(Longword(SP) - Longword(FI^.lpSprites)) div SizeOf(TSprite);

  i:= idx;
  k:= idx + 1;

  cur:= PSprite(Longword(FI^.lpSprites) + i*SizeOf(TSprite));
  next:= PSprite(Longword(FI^.lpSprites) + k*SizeOf(TSprite));

  Move(SP^, tmp, SizeOf(TSprite));
  while (k < MAX_SPRITES) do begin
    move(next^, cur^, SizeOf(TSprite));
    cur^.spFlags:= cur^.spFlags or SP_REPAINT;

    Inc(i);
    Inc(k);
    Inc(cur);
    Inc(next);
  end;

  if i <> idx then begin
    Move(tmp, cur^, SizeOf(TSprite));
    cur^.spFlags:= cur^.spFlags or SP_REPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp508(Img: Longword; var aWidth, aHeight: Integer); stdcall;
{$else}
procedure spImgGetDimensions(Img: Longword; var aWidth, aHeight: Integer); stdcall;
{$endif}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(Img);
  if SP <> nil then begin
		aWidth:= SP^.spMaxWidth;
		aHeight:= SP^.spMaxHeight;
	end;
end;

{$ifdef RELEASE}
function sp509(hImg: Longword): HWND; stdcall;
{$else}
function spImgGetParent(hImg: Longword): HWND; stdcall;
{$endif}
var
  SP: PSprite;
begin
  Result:= INVALID_HANDLE_VALUE;
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  Result:= SP^.spParent;
end;

{$ifdef RELEASE}
procedure sp510(hImg: Longword; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spImgGetPosition(hImg: Longword; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;
	
	with SP^ do begin
		lpLeft   := spLeft;
		lpTop    := spTop;
    lpWidth  := spWidth;
		lpHeight := spHeight;
	end;
end;

{$ifdef RELEASE}
function sp511(hImg: Longword): Integer; stdcall;
{$else}
function spImgGetRotateAngle(hImg: Longword): Integer; stdcall;
{$endif}
var
  SP: PSprite;
begin
  Result:= 0;
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;
	
	Result:= SP^.spRotate + 1;
end;

{$ifdef RELEASE}
function sp512(hImg: Longword): Integer; stdcall;
{$else}
function spImgGetSpriteCount(hImg: Longword): Integer; stdcall;
{$endif}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);

  if SP <> nil then Result:= SP^.spMaxIndex
  else Result:= 0;
end;

{$ifdef RELEASE}
function sp513(hImg: Longword): Integer; stdcall;
{$else}
function spImgGetSpriteIndex(hImg: Longword): Integer; stdcall;
{$endif}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);

  if SP <> nil then Result:= SP^.spIndex
	else Result:= 0;
end;

{$ifdef RELEASE}
function sp514(hImg: Longword): Integer; stdcall;
{$else}
function spImgGetTransparent(hImg: Longword): Integer; stdcall;
{$endif}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Result:= 0
  else Result:= SP^.splwaAlpha;
end;

{$ifdef RELEASE}
function sp515(hImg: Longword): Boolean; stdcall;
{$else}
function spImgGetVisibility(hImg: Longword): Boolean; stdcall;
{$endif}
var
	SP: PSprite;
begin
  Result:= False;
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  Result:= SP^.spShowImg;
end;

{$ifdef RELEASE}
procedure sp516(hImg: Longword; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spImgGetVisiblePart(hImg: Longword; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

	with SP^ do begin
		lpLeft   := spPartLeft;
		lpTop    := spPartTop;
		lpWidth  := spPartWidth;
		lpHeight := spPartHeight;
	end;
end;

{$ifdef RELEASE}
function sp517(hImg: Longword): Integer; stdcall;
{$else}
function spImgGetVisiblePie(hImg: Longword): Integer; stdcall;
{$endif}
var
	SP: PSprite;
begin
	Result:=0;
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

	Result:=SP^.spVisiblePie;
end;

{$ifdef RELEASE}
procedure sp518(hImg: Longword; aColor: Integer); stdcall;
{$else}
procedure spImgSetBackgroundColor(hImg: Longword; aColor: Integer); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

	with SP^ do begin
    if aColor <> -1 then spCutColor:= ($FF shl 24) or aColor
    else spCutColor:= aColor;

    spFlags:= spFlags or SP_REPAINT;
	end;
end;

{$IFDEF RELEASE}
procedure sp519(hImg: Longword; iTime: Longword); stdcall;
{$ELSE}
procedure spImgSetGifIdleTime(hImg: Longword; iTime: Longword); stdcall;
{$ENDIF}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  SP^.spGifIdleTime:= iTime;
end;

{$IFDEF RELEASE}
procedure sp520(hImg: Longword; iTime: Longword); stdcall;
{$ELSE}
procedure spImgSetGifInterval(hImg: Longword; iTime: Longword); stdcall;
{$ENDIF}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  with SP^ do begin
    spGifInterval:= iTime;
    SetTimer(spParent, spGifTimer, iTime, @spGifTimerProc);
  end;
end;

{$ifdef RELEASE}
procedure sp521(hImg: Longword; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spImgSetPosition(hImg: Longword; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

	with SP^ do begin
    if (spLeft = lpLeft) and (spTop = lpTop) and
       (spWidth = lpWidth)and (spHeight = lpHeight) then Exit;
			 
    Move((@spLeft)^, (@spOldLeft)^, 16);
		spLeft:= lpLeft;
		spTop:= lpTop;
		
		if lpWidth < 0 then spWidth:= 0 else spWidth:= lpWidth;
		if lpHeight < 0 then spHeight:= 0 else spHeight:= lpHeight;

    if (spOldLeft <> spLeft) or (spOldTop <> spTop) or
       (spOldWidth <> spWidth) or (spOldHeight <> spHeight) then spFlags:= spFlags or SP_IMGMOVED;
			 
    spFlags:= spFlags or SP_NEEDPAINT;
	end;	
end;

{$ifdef RELEASE}
procedure sp522(hImg: Longword; lpAngle: Integer); stdcall;
{$else}
procedure spImgSetRotateAngle(hImg: Longword; lpAngle: Integer); stdcall;
{$endif}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;
	
  with SP^ do begin
		if (spRotate = 0) and (spOldRotate = 0) and (lpAngle = 1) then lpAngle:= 1;
		spOldRotate:= spRotate;
		spRotate:= lpAngle-1;
			
		spFlags:= spFlags or SP_REPAINT;
	end;
end;

{$ifdef RELEASE}
procedure sp523(hImg: Longword; lpSpriteCount: Integer); stdcall;
{$else}
procedure spImgSetSpriteCount(hImg: Longword; lpSpriteCount: Integer); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

	with SP^ do begin
	{my_add}
		if lpSpriteCount < 1 then lpSpriteCount:= 1;
		if (spFlags and SP_STRETCH = 0) and (spMaxIndex > 1) and (lpSpriteCount = 1) then begin
			spFlags     := spFlags or SP_ISSPRITE;
			spMaxWidth  := spImgWidth;
			spMaxHeight := spImgHeight;
			spPartLeft  := 0;
			spPartTop   := 0;
			spPartWidth := spWidth;
			spPartHeight:= spHeight;
			spIndex     := 0;
			spMaxIndex  := lpSpriteCount;
		end else begin
	{end}
			spFlags     := spFlags or SP_ISSPRITE;
			spMaxWidth  := spImgWidth;
			spMaxHeight := spImgHeight div lpSpriteCount;
			spPartLeft  := 0;
			spPartTop   := 0;
			spPartWidth := spMaxWidth;
			spPartHeight:= spMaxHeight;
			spIndex     := 0;
			spMaxIndex  := lpSpriteCount;
		end;
	end;
end;

{$ifdef RELEASE}
procedure sp524(hImg: Longword; lpIndex: Integer); stdcall;
{$else}
procedure spImgSetSpriteIndex(hImg: Longword; lpIndex: Integer); stdcall;
{$endif}
var
  SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
	if SP = nil then Exit;
	
	with SP^ do begin
    if spIndex = lpIndex then Exit;
    if lpIndex <= 0 then lpIndex:= 1;
    if lpIndex > spMaxIndex then lpIndex:= spMaxIndex;

    spIndex:= lpIndex;
    spPartTop:= (spIndex-1)*spMaxHeight;
    spFlags:= spFlags or SP_NEEDPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp525(hImg: Longword; aAlpha: Integer); stdcall;
{$else}
procedure spImgSetTransparent(hImg: Longword; aAlpha: Integer); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  with SP^ do begin
    if aAlpha < 0 then aAlpha:= 0;
    if aAlpha > 255 then aAlpha:= 255;
    if splwaAlpha <> aAlpha then begin
		  splwaAlpha:= aAlpha;
      spFlags:= spFlags or SP_REPAINT;
    end;
	end;
end;

{$ifdef RELEASE}
procedure sp526(hImg: Longword; lpVisible: Boolean); stdcall;
{$else}
procedure spImgSetVisibility(hImg: Longword; lpVisible: Boolean); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;
	
	with SP^ do begin
		spShowImg:= lpVisible;
    spFlags:= spFlags or SP_NEEDPAINT;
	end;	
end;

{$ifdef RELEASE}
procedure sp527(hImg: Longword; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spImgSetVisiblePart(hImg: Longword; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

	if lpLeft < 0 then lpLeft:= 0;
	if lpTop < 0 then lpTop:= 0;
	
	with SP^ do begin
	//if spMaxIndex > 1 then Exit; {my_add}
    if (lpLeft = spPartLeft) and (lpTop = spPartTop) and
       (lpWidth = spPartWidth) and (lpHeight = spPartHeight) then Exit;

		if lpLeft > spMaxWidth then lpLeft:= spMaxWidth;
		if lpTop > spMaxHeight then lpTop:= spMaxHeight;
		if lpWidth > spMaxWidth then lpWidth:= spMaxWidth-lpLeft;
		if lpHeight > spMaxHeight then lpHeight:= spMaxHeight-lpTop;

		spPartLeft:= lpLeft;
		
		if spFlags and SP_ISSPRITE <> 0 then
			spPartTop:= ((spIndex-1)*spMaxHeight)+lpTop else spPartTop:= lpTop;
    
    if spFlags and SP_STRETCH <> 0 then begin {my_add old "="}
		  spPartWidth:= lpWidth;
		  spPartHeight:= lpHeight;
    end else begin
		  spPartWidth:= spWidth;
		  spPartHeight:= spHeight;
    end;
    spFlags:= spFlags or SP_NEEDPAINT;
	end;
end;

{$ifdef RELEASE}
procedure sp528(hImg: Longword; lpVisAngle: Integer); stdcall;
{$else}
procedure spImgSetVisiblePie(hImg: Longword; lpVisAngle: Integer); stdcall;
{$endif}
var
	SP: PSprite;
begin
  SP:= spIsValidSprite(hImg);
  if SP = nil then Exit;

  with SP^ do begin
    if lpVisAngle > 360 then spVisiblePie:= 0 else
		if (spVisiblePie = -1) then begin
			if lpVisAngle = 0 then spVisiblePie:= 0 else spVisiblePie:= lpVisAngle;
		end else spVisiblePie:= lpVisAngle;

		if spVisiblePie < -1 then spVisiblePie:=-1;

    spFlags:= spFlags or SP_REPAINT;
  end;
end;

exports
{$ifdef RELEASE}
{dll}
	sp101, sp102,
{win}
	sp201, sp202, sp203, sp204,	
{img}
	sp501, sp502, sp503, sp504, sp505,
{*}
	sp506, sp507,
{get}
	sp508, sp509, sp510, sp511, sp512, sp513, sp514, sp515, sp516, sp517,
{set}
  sp518, sp519, sp520, sp521, sp522, sp523, sp524, sp525, sp526, sp527, sp528;
{$else}
{dll}
	spInitialize,
	spShutdown,
{win}
	spImageFormCreate,
	spImageFormSetAlpha,
	spImageFormUpdate,
	spApplyChanges,	
{img}
	spImgLoadImage,
	spImgLoadImageFromBuffer,
	spImgLoadImageFromResourceName,
	spImgLoadImageFromResourceLibrary,
	spImgRelease,
{*}
	spImgBringToFront,
	spImgSendToBack,
{get}
	spImgGetDimensions,
	spImgGetParent,
	spImgGetPosition,
	spImgGetRotateAngle,
	spImgGetSpriteCount,
	spImgGetSpriteIndex,
	spImgGetTransparent,
	spImgGetVisibility,
	spImgGetVisiblePart,
	spImgGetVisiblePie,
{set}
  spImgSetBackgroundColor,
	spImgSetGifIdleTime,
	spImgSetGifInterval,
	spImgSetPosition,
	spImgSetRotateAngle,
	spImgSetSpriteCount,
	spImgSetSpriteIndex,
	spImgSetTransparent,
	spImgSetVisibility,
	spImgSetVisiblePart,
	spImgSetVisiblePie;
{$endif}

begin
  ISSpriteMain.InfoPoint    := nil;
  ISSpriteMain.InfoCount    := MAX_INFO;
  ISSpriteMain.ISSpriteInit := False;
	ISSpriteMiniature			 		:= False;
end.