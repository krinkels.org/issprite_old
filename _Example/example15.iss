#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: callbackctrl.dll; DestDir: {tmp}; Flags: dontcopy
Source: KrinkelsTeam.png; DestDir: {tmp}; Flags: dontcopy
Source: 5.png; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;
Source: button.png; DestDir: {tmp}; Flags: dontcopy
Source: Box.png; DestDir: {tmp}; Flags: dontcopy

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';
function ReleaseCapture(): Longint; external 'ReleaseCapture@user32.dll stdcall';

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
begin
  spImgUpdateImageForm(WizardForm.Handle);
end;

procedure btnOnClick(hBtn: HWND; nEventID: Word);
begin
  MsgBox('Button Clicked', mbConfirmation, MB_OK);
end;

procedure WizardFormOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(WizardForm.Handle,$0112,$F012,0);
end;

var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer; tmr: Longword;
  h1, h2: HWND;
var
  fnt: Longword;
  
procedure InitializeWizard();
var
  l: Longword;
begin
  spInitialize(true, true);
  
  ExtractTemporaryFile('KrinkelsTeam.png');
  ExtractTemporaryFile('5.png');
  ExtractTemporaryFile('button.png');
  ExtractTemporaryFile('box.png');
  
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;
  WizardForm.BorderStyle:= bsNone;
  WizardForm.Color:= $FFFF00;
  WizardForm.Width:= 500;
  WizardForm.OnMouseDown:= @WizardFormOnMouseDown;

  WizardForm.DirEdit.parent:= WizardForm;
  WizardForm.DirEdit.Font.Color:= clRed;
  Sp1:= spImgLoadImage(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\Krinkelsteam.png')), 0, 0, 500, 258, True, True);
  Sp2:= spImgLoadImage(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\5.png')), 80, 260, 100, 100, True, False);

  spImgCreateFormFromImage(WizardForm.Handle, SP1, false);

  fnt:= spFntCreateFont(PAnsiChar(WizardForm.Font.Name), false, false, false, false, 10);
  l:= spShdAddText(WizardForm.Handle, 0, 240, ScaleX(425), ScaleY(70), '���� ���� ����� ����������� � [b][min=200][color=$FF0000]'+WizardForm.DirEdit.Text+'\Local\AppData\Server host[/color][/b][/min] � ��� ��������� ����������� [i][color=$FF0000]9.98 ��[/color][/i] ���������� ��������� ������������.', $EEEEEE, fnt);
  spShdAddGradient(l, $0000FF, $FF00FF, $10);

  h1:= spBtnCreateButton(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\button.png')), 20, 200, 120, 32, $20, 12);
  spBtnSetEvent(h1, btnMouseClickEvent, WrapButtonProc(@btnOnClick, 1));
  spBtnSetFontColor(h1, $FFFFFF, $FF6666, $6666FF, $888888);
  spBtnSetText(h1, 'TestButton');

  spBtnSetCursor(h1, spBtnGetSysCursor(32649));

  h2:= spBtnCreateButton(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\box.png')), 160, 100, 16, 16, $80, 0);
  spBtnSetChecked(h2, 1);
//  spBtnRefresh(h3);
//  spBtnRefresh(h1);

  spApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 80, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  spFntFreeFont(fnt);
  spShutdown;
end;