#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: innocallback.dll; DestDir: {tmp}; Flags: dontcopy
Source: 6766.bmp; DestDir: {tmp}; Flags: dontcopy
Source: 7f77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: 7sf77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallback@files:innocallback.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';

var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer; tmr: Longword;

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
begin
  MoveSpriteHor(SP2, 2, 25, 472);
  spImgSetSpriteIndex(SP2, n2);

  MoveSpriteHor(SP3, 2, 25, 472);
  spImgSetSpriteIndex(SP3, n2);

  n2:=n2+1; if n2=16 then n2:=1;
  n3:=n3+1; if n3=16 then n3:=1;
  spImgApplyChanges(WizardForm.handle);
end;

var
  fnt: Longword;
  
procedure InitializeWizard();
var
  l: Longword;
begin
  spInitialize();
  
  ExtractTemporaryFile('6766.bmp');
  ExtractTemporaryFile('7f77.bmp');
  ExtractTemporaryFile('7sf77.bmp');

  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;

  with TLabel.Create(WizardForm) do begin
    with Font do begin
      Color:=clRed;
      Style:=[fsBold, fsItalic];
      Size:=14;
    end;
    Caption:='TEST Label';
    Left:=150;
    Top:=25;
    Parent:=WizardForm;
    Transparent:=True;
  end;

  Sp1:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\6766.bmp')), 0, 0, 497, 360, -1, True, True);
  Sp2:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7f77.bmp')), 30, 6, 28, 40, $FF00FF, False, False);
  Sp3:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7sf77.bmp')), 120, -3, 48, 53, $FF00FF, False, False);
  spImgSetSpriteCount(SP2, 15);
  spImgSetSpriteCount(SP3, 15);
//  WizardForm.InnerNotebook.Hide;
//  WizardForm.OuterNotebook.Hide;
  fnt:= spFntCreateFont(PAnsiChar(WizardForm.Font.Name), false, false, false, false, 10);
  l:= spShdAddText(WizardForm.Handle, 0, 100, ScaleX(425), ScaleY(70), '���� ���� ����� ����������� � [b][min=200][color=$FF0000]'+WizardForm.DirEdit.Text+'[/color][/b][/min] � ��� ��������� ����������� [i][color=$FF0000]9.98 ��[/color][/i] ���������� ��������� ������������.', 4, 1, $EEEEEE, $AAAAAA, fnt);
//  shdAddGradient(l, $0000FF, $FF00FF, 2);
  spImgApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 80, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  spFntFreeFont(fnt);
  spShutdown;
end;