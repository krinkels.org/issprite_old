#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: innocallback.dll; DestDir: {tmp}; Flags: dontcopy
Source: 1.jpg; DestDir: {tmp}; Flags: dontcopy
Source: 5.png; DestDir: {tmp}; Flags: dontcopy
Source: 7f77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: 7sf77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallback@files:innocallback.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';

var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer; tmr: Longword;

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
begin
  MoveSpriteHor(SP2, 2, 25, 472);
  bmpSetSpriteIndex(SP2, n2);

  MoveSpriteHor(SP3, 2, 25, 472);
  bmpSetSpriteIndex(SP3, n2);
  
  n2:=n2+1; if n2=16 then n2:=1;
  n3:=n3+1; if n3=16 then n3:=1;
  bmpApplyChanges(WizardForm.handle);
end;

procedure InitializeWizard();
begin
  ExtractTemporaryFile('1.jpg');
  ExtractTemporaryFile('5.png');
  ExtractTemporaryFile('7f77.bmp');
  ExtractTemporaryFile('7sf77.bmp');
  
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;

  with TLabel.Create(WizardForm) do begin
    with Font do begin
      Color:=clRed;
      Style:=[fsBold, fsItalic];
      Size:=14;
    end;
    Caption:='TEST Label';
    Left:=150;
    Top:=25;
    Parent:=WizardForm;
    Transparent:=True;
  end;

  Sp1:= bmpLoadImg(WizardForm.Handle, PChar(ExpandConstant('{tmp}\1.jpg')), 0, 0, 497, 360, -1, True, True);
  Sp1:= bmpLoadImg(WizardForm.Handle, PChar(ExpandConstant('{tmp}\5.png')), 80, 100, 100, 100, -1, True, True);
  Sp2:= bmpLoadImg(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7f77.bmp')), 30, 6, 28, 40, $FF00FF, False, False);
  Sp3:= bmpLoadImg(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7sf77.bmp')), 120, -3, 48, 53, $FF00FF, False, False);
  bmpSetSpriteMaxIndex(SP2, 15);
  bmpSetSpriteMaxIndex(SP3, 15);
  tmr:= SetTimer(0, 0, 80, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  bmpShutdown;
end;
