#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: callbackctrl.dll; DestDir: {tmp}; Flags: dontcopy
Source: KrinkelsTeam.png; DestDir: {tmp}; Flags: dontcopy
Source: 5.png; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';
function ReleaseCapture(): Longint; external 'ReleaseCapture@user32.dll stdcall';

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
begin
  spImgUpdateImageForm(WizardForm.Handle);
end;

procedure WizardFormOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(WizardForm.Handle,$0112,$F012,0);
end;

var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer; tmr: Longword;

var
  fnt: Longword;
  
procedure InitializeWizard();
var
  l: Longword;
begin

  ExtracttemporaryFile('ISSprite.dll');
  
  spInitialize(true, true);
  
  ExtractTemporaryFile('KrinkelsTeam.png');
  ExtractTemporaryFile('5.png');

  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;
  WizardForm.BorderStyle:= bsNone;
  WizardForm.Color:= $FFFF00;
  WizardForm.Width:= 500;
  WizardForm.OnMouseDown:= @WizardFormOnMouseDown;

  Sp1:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\Krinkelsteam.png')), 0, 0, 500, 258, True, True);
  Sp2:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\5.png')), 80, 260, 100, 100, True, False);

  //spImgCreateFormFromImage(WizardForm.Handle, SP1, false);

  spImgCreateImageForm(WizardForm.handle, false);
  
  fnt:= spFntCreateFont(PAnsiChar(WizardForm.Font.Name), 0, false, false, false, 10);
//  l:= spShdAddText(WizardForm.Handle, 0, 240, ScaleX(425), ScaleY(70), '���� ���� ����� ����������� � [b][min=200][color=$FF0000]'+WizardForm.DirEdit.Text+'[/color][/b][/min] � ��� ��������� ����������� [i][color=$FF0000]9.98 ��[/color][/i] ���������� ��������� ������������.', $000000, fnt);
//  l:= spShdAddText(WizardForm.Handle, ScaleX(0), ScaleY(35), ScaleX(500), ScaleY(70), '[align=tacenter][b]���[/b] [i]������������ ������[/i] ��������� ���� [color=$002BFF2B]�dgfdgfd�[/color][/align]', $000000, fnt);
//  l:= spShdAddText(WizardForm.Handle, ScaleX(0), ScaleY(35), ScaleX(500), ScaleY(70), '[align=tacenter][size=20]���[/size] ������������ ������ ��������� ���� [color=$002BFF2B]�dgfdgfd�[/color][/align]', $000000, fnt);
//  l:= spShdAddText(WizardForm.Handle, ScaleX(0), ScaleY(35), ScaleX(500), ScaleY(70), '[align=taright][b]���[/b] [i]������������ [u]������[/u][/i] [name=Georgia]���������[/name] [s]����[/s][color=$002BFF2B][size=15] �dgfdgfd�[/size][/color][/align]', $000000, fnt);
  l:= spShdAddText(WizardForm.Handle, ScaleX(0), ScaleY(35), ScaleX(500), ScaleY(70), '[min=50][align=taleft][u][i]��� ������������ ������[/min] [font=Georgia]���������[/font] ����[color=$0000ff][size=15] �[Prototype]�[/size][/u][/i][/color][/align]', $000000, fnt);
  //spShdAddGradient(l, $0000FF, $FF00FF, 2);

  spApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 80, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  spFntFreeFont(fnt);
  spShutdown;
end;