#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: callbackctrl.dll; DestDir: {tmp}; Flags: dontcopy
Source: 6766.bmp; DestDir: {tmp}; Flags: dontcopy
Source: 7f77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: 7sf77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: button.png; DestDir: {tmp}; Flags: dontcopy
Source: box.png; DestDir: {tmp}; Flags: dontcopy
Source: CheckBox.png; DestDir: {tmp}; Flags: dontcopy
Source: KrinkelsTeam.png; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);
  
function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';

var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer; tmr: Longword;
  h1, h2, h3: HWND;


procedure btnOnClick(hBtn: HWND);
begin
  MsgBox('Button Clicked', mbConfirmation, MB_OK);
end;

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
var
  x, y, xx, yy: Integer;
begin
  spBtnGetPos(h1, x, y, xx, yy);
  spBtnSetPos(h1, x+2, y, xx, yy);
//  spBtnRefresh(h1);
{  MoveSpriteHor(SP2, 2, 25, 472);
  spImgSetSpriteIndex(SP2, n2);

  MoveSpriteHor(SP3, 2, 25, 472);
  spImgSetSpriteIndex(SP3, n2);

  n2:=n2+1; if n2=16 then n2:=1;
  n3:=n3+1; if n3=16 then n3:=1;
  spApplyChanges(WizardForm.handle);  }
end;

procedure InitializeWizard();
begin
  spInitialize(true);
  
  ExtractTemporaryFile('6766.bmp');
  ExtractTemporaryFile('7f77.bmp');
  ExtractTemporaryFile('7sf77.bmp');
  ExtractTemporaryFile('button.png');
  ExtractTemporaryFile('box.png');
  ExtractTemporaryFile('checkbox.png');
  ExtractTemporaryFile('KrinkelsTeam.png');
  
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;

  with TLabel.Create(WizardForm) do begin
    with Font do begin
      Color:=clRed;
      Style:=[fsBold, fsItalic];
      Size:=14;
    end;
    Caption:='TEST Label';
    Left:=150;
    Top:=25;
    Parent:=WizardForm;
    Transparent:=True;
  end;

  Sp1:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\6766.bmp')), 0, 0, 497, 360, -1, True, True);
  Sp2:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7f77.bmp')), 30, 6, 28, 40, $FF00FF, False, False);
  Sp3:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7sf77.bmp')), 120, -3, 48, 53, $FF00FF, False, False);
  spImgSetSpriteCount(SP2, 15);
  spImgSetSpriteCount(SP3, 15);

  h1:= spBtnCreateButton(WizardForm.Handle, PChar(ExpandConstant('{tmp}\button.png')), 20, 100, 120, 32, $20, 12);
  h2:= spBtnCreateButton(WizardForm.Handle, PChar(ExpandConstant('{tmp}\KrinkelsTeam.png')), 20, 200, 300, 160, $10, 0);
  spBtnSetEvent(h1, btnMouseClickEvent, WrapButtonProc(@btnOnClick, 1));
  spBtnSetFontColor(h1, $FFFFFF, $FF6666, $6666FF, $888888);
  spBtnSetText(h1, 'TestButton');
  
  spBtnSetCursor(h2, spBtnGetSysCursor(32649));
  
  h3:= spBtnCreateButton(WizardForm.Handle, PChar(ExpandConstant('{tmp}\box.png')), 160, 100, 16, 16, $80, 0);
  spBtnSetChecked(h3, 1);
//  spBtnRefresh(h3);
//  spBtnRefresh(h1);
  spApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 50, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
//  KillTimer(0, Tmr);
  spShutdown;
end;
