;ISSprite v.0.0.7.47 build #31
[code]
type
  TCommonMouseEvent       = procedure(hObject: Longword; nEventID: Word);
  TImgButtonEvent         = procedure(hWnd: HWND; nEventID: Word);
  TShadowTextEvent        = procedure(hImg: Longword; nEventID: Word);

const
  SHD_GRADIENTVER         = $0020;
  SHD_GRADIENTHOR         = $0010;

  SHD_STYLEGLOW           = $0080;
  SHD_STYLESHADOW         = $0040;
  
  cmnMouseClickEvent      = 1;
  cmnMouseEnterEvent      = 2;
  cmnMouseLeaveEvent      = 3;
  cmnMouseMoveEvent       = 4;
  cmnMouseDownEvent       = 5;
  cmnMouseUpEvent         = 6;

  btnMouseClickEvent      = cmnMouseClickEvent;
  btnMouseEnterEvent      = cmnMouseEnterEvent;
  btnMouseLeaveEvent      = cmnMouseLeaveEvent;
  btnMouseMoveEvent       = cmnMouseMoveEvent;
  btnMouseDownEvent       = cmnMouseDownEvent;
  btnMouseUpEvent         = cmnMouseUpEvent;

  shdMouseClickEvent      = cmnMouseClickEvent;
  shdMouseEnterEvent      = cmnMouseEnterEvent;
  shdMouseLeaveEvent      = cmnMouseLeaveEvent;
  shdMouseMoveEvent       = cmnMouseMoveEvent;
  shdMouseDownEvent       = cmnMouseDownEvent;
  shdMouseUpEvent         = cmnMouseUpEvent;

  btnAlignDefault         = 1;
  btnAlignTopLeft         = 2;
  btnAlignBottomLeft      = 3;
  btnAlignTopRight        = 4;
  btnAlignBottomRight     = 5;
  btnAlignTopCenter       = 6;
  btnAlignBottomCenter    = 7;
  btnAlignCenter          = 8;
  
  BTN_PANEL               = $10;
  BTN_BUTTON              = $20;
  BTN_CHECKBOX            = $40;
  BTN_CHECKBOXEX          = $80;
  BTN_TAB                 = $100;
  BTN_ANIMATE             = $200; //not working yet

  CS_ANSI_CHARSET         = 0;
  CS_DEFAULT_CHARSET      = 1;
  CS_SYMBOL_CHARSET       = 2;
  CS_SHIFTJIS_CHARSET     = $80;
  CS_HANGEUL_CHARSET      = 129;
  CS_GB2312_CHARSET       = 134;
  CS_CHINESEBIG5_CHARSET  = 136;
  CS_OEM_CHARSET          = 255;
  CS_JOHAB_CHARSET        = 130;
  CS_HEBREW_CHARSET       = 177;
  CS_ARABIC_CHARSET       = 178;
  CS_GREEK_CHARSET        = 161;
  CS_TURKISH_CHARSET      = 162;
  CS_VIETNAMESE_CHARSET   = 163;
  CS_THAI_CHARSET         = 222;
  CS_EASTEUROPE_CHARSET   = 238;
  CS_RUSSIAN_CHARSET      = 204;

  CS_MAC_CHARSET          = 77;
  CS_BALTIC_CHARSET       = 186;

//1
function spImgLoadImage(lpParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'spImgLoadImage@{tmp}\ISSprite.dll stdcall delayload';
//2
function spImgLoadImageFromBuffer(lpParent: HWND; lpBuffer: Longint; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'spImgLoadImageFromBuffer@{tmp}\ISSprite.dll stdcall delayload';
//3
function spImgLoadImageFromResourceLibrary(lpParent: HWND; lpResLibrary: PAnsiChar; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'spImgLoadImageFromResourceLibrary@{tmp}\ISSprite.dll stdcall delayload';
//4
function spImgLoadImageFromResourceName(lpParent: HWND; lpInstance: longword; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'spImgLoadImageFromResourceName@{tmp}\ISSprite.dll stdcall delayload';
//5
procedure spImgRelease(hImg: Longword);
  external 'spImgRelease@{tmp}\ISSprite.dll stdcall delayload';
//6
function spImgCreateImageForm(hWnd: HWND; nSetTimer: Boolean): Boolean;
  external 'spImgCreateImageForm@{tmp}\IsSprite.dll stdcall delayload';
//7
procedure spImgUpdateImageForm(hWnd: HWND);
  external 'spImgUpdateImageForm@{tmp}\IsSprite.dll stdcall delayload';
//8
procedure spImgSetImageFormAlpha(hWnd: HWND; lpAlpha: Byte);
  external 'spImgSetImageFormAlpha@{tmp}\IsSprite.dll stdcall delayload';
//9
procedure spImgGetDimensions(hImg: longword; var Width, Height: Integer);
  external 'spImgGetDimensions@{tmp}\ISSprite.dll stdcall delayload';
//10
procedure spImgSetGifInterval(hImg: Longword; iTime: Longword);
  external 'spImgSetGifInterval@{tmp}\ISSprite.dll stdcall delayload';
//11
procedure spImgSetGifIdleTime(hImg: Longword; iTime: Longword);
  external 'spImgSetGifIdleTime@{tmp}\ISSprite.dll stdcall delayload';
//12
procedure spImgSetRotateAngle(hImg: Longword; lpAngle: Integer);
  external 'spImgSetRotateAngle@{tmp}\ISSprite.dll stdcall delayload';
//13
function spImgGetRotateAngle(hImg: Longword): Integer;
  external 'spImgGetRotateAngle@{tmp}\ISSprite.dll stdcall delayload';
//14
procedure spImgSetBackgroundColor(hImg: longword; aColor: integer);
  external 'spImgSetBackgroundColor@{tmp}\ISSprite.dll stdcall delayload';
//15
procedure spImgSetTransparent(hImg: Longword; aAlpha: Integer);
  external 'spImgSetTransparent@{tmp}\ISSprite.dll stdcall delayload';
//16
function spImgGetTransparent(hImg: Longword): Integer;
  external 'spImgGetTransparent@{tmp}\ISSprite.dll stdcall delayload';
//17
function spImgGetParent(hImg: Longword): HWND;
  external 'spImgGetParent@{tmp}\ISSprite.dll stdcall delayload';
//18
procedure spImgSetVisibility(hImg: longword; Visible: Boolean);
  external 'spImgSetVisibility@{tmp}\ISSprite.dll stdcall delayload';
//19
function spImgGetVisibility(hImg: longword): Boolean;
  external 'spImgGetVisibility@{tmp}\ISSprite.dll stdcall delayload';
//20
procedure spImgSetPos(hImg: Longword; ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgSetPos@{tmp}\ISSprite.dll stdcall delayload';
//21
procedure spImgGetPos(hImg: Longword; var ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgGetPos@{tmp}\ISSprite.dll stdcall delayload';
//22
procedure spImgSetVisiblePart(hImg: Longword; ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgSetVisiblePart@{tmp}\ISSprite.dll stdcall delayload';
//23
procedure spImgGetVisiblePart(hImg: Longword; var ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgGetVisiblePart@{tmp}\ISSprite.dll stdcall delayload';
//24
procedure spImgSetVisiblePie(hImg: Longword; lpVisAngle: Integer);
  external 'spImgSetVisiblePie@{tmp}\ISSprite.dll stdcall delayload';
//25
procedure spImgSetSpriteCount(hImg: Longword; SpriteCount: Integer);
  external 'spImgSetSpriteCount@{tmp}\ISSprite.dll stdcall delayload';
//26
function spImgGetSpriteCount(hImg: Longword): Integer;
  external 'spImgGetSpriteCount@{tmp}\ISSprite.dll stdcall delayload';
//27
procedure spImgSetSpriteIndex(hImg: Longword; Index: Integer);
  external 'spImgSetSpriteIndex@{tmp}\ISSprite.dll stdcall delayload';
//28
function spImgGetSpriteIndex(hImg: Longword): Integer;
  external 'spImgGetSpriteIndex@{tmp}\ISSprite.dll stdcall delayload';
//29;
procedure spImgBringToFront(hImg: Longword);
  external 'spImgBringToFront@{tmp}\ISSprite.dll stdcall delayload';
//30
procedure spImgSendToBack(hImg: Longword);
  external 'spImgSendToBack@{tmp}\ISSprite.dll stdcall delayload';
//-- ###################################################################################### -- //

//1
procedure spApplyChanges(h: HWND);
  external 'spApplyChanges@{tmp}\ISSprite.dll stdcall delayload';
//2
procedure spInitialize(nUseLogicPixels: Boolean; nUseSmartPaint: Boolean);
  external 'spInitialize@{tmp}\ISSprite.dll stdcall delayload';
//3
procedure spShutdown();
  external 'spShutdown@{tmp}\ISSprite.dll stdcall delayload';


//-- ###################################################################################### -- //

//1
function spShdAddText(lpParent: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpCaption: PAnsiChar; lpTextColor: Integer; aFont: Longword): Longword;
  external 'spShdAddText@{tmp}\ISSprite.dll stdcall delayload';
//2
function spShdGetparent(hText: Longword): HWND;
  external 'spShdGetParent@{tmp}\ISSprite.dll stdcall delayload';
//3
procedure spShdSetText(hText: Longword; lpCaption: PAnsiChar);
  external 'spShdSetText@{tmp}\ISSprite.dll stdcall delayload';
//4
procedure spShdSetShadow(hText: Longword; lpShadowColor: Cardinal; lpShadowSize, lpShadowIntense: Byte; lpShadowStyle: DWORD);
  external 'spShdSetShadow@{tmp}\ISSprite.dll stdcall delayload';
//5
procedure spShdSetRotateAngle(hText: Longword; lpAngle: Integer);
  external 'spShdSetRotateAngle@{tmp}\ISSprite.dll stdcall delayload';
//6
procedure spShdSetVisible(hText: Longword; lpVisible: Boolean);
  external 'spShdSetVisible@{tmp}\ISSprite.dll stdcall delayload';
//7
function spShdGetVisible(hText: Longword): Boolean;
  external 'spShdGetVisible@{tmp}\ISSprite.dll stdcall delayload';
//8
procedure spShdSetEvent(hText: Longword; lpEventID: Integer; lpEvent: TShadowTextEvent);
  external 'spShdSetEvent@{tmp}\ISSprite.dll stdcall delayload';
//9
procedure spShdAddGradient(hText: Longword; lpBeginColor, lpEndColor: Cardinal; lpStyle: DWORD);
  external 'spShdAddGradient@{tmp}\ISSprite.dll stdcall delayload';
//10
procedure spShdFreeGradient(hText: Longword);
  external 'spShdFreeGradient@{tmp}\ISSprite.dll stdcall delayload';
//11
procedure spShdSetTextColor(hText: Longword; lpTextColor: Cardinal);
  external 'spShdSetTextColor@{tmp}\ISSprite.dll stdcall delayload';
//12
function spShdGetTextColor(hText: Longword): TColor;
  external 'spShdGetTextColor@{tmp}\ISSprite.dll stdcall delayload';
//13
procedure spShdSetShadowColor(hText: Longword; lpShadowColor: Cardinal);
  external 'spShdSetShadowColor@{tmp}\ISSprite.dll stdcall delayload';
//14
function spShdGetShadowColor(hText: Longword): TColor;
  external 'spShdGetShadowColor@{tmp}\ISSprite.dll stdcall delayload';
//15
procedure spShdSetCharacterExtra(hText: Longword; lpCharExtra: Integer);
  external 'spShdSetCharacterExtra@{tmp}\ISSprite.dll stdcall delayload';
//16
procedure spShdSetPos(hText: Longword; lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spShdSetPos@{tmp}\ISSprite.dll stdcall delayload';
//17
procedure spShdGetPos(hText: Longword; var lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spShdGetPos@{tmp}\ISSprite.dll stdcall delayload';
//18
procedure spShdSetTag(hText: Longword; lpText: PAnsiChar);
  external 'spShdSetTag@{tmp}\ISSprite.dll stdcall delayload';
//19
procedure spShdGetTag(hText: Longword;var  lpText: PAnsiChar; iBuffSize: Longint);
  external 'spShdSetTag@{tmp}\ISSprite.dll stdcall delayload';
  
//-- ###################################################################################### -- //

//1
function spFntCreateFont(lpFontName: PAnsiChar; lpBold: Longint; lpItalic, lpUnderline, lpStrikeOut: Boolean; lpFontSize: Integer): Longword;
  external 'spFntCreateFont@{tmp}\ISSprite.dll stdcall delayload';
//2
procedure spFntChangeCharSet(var lpFont: Longword; lpCharSet: DWORD);
  external 'spFntChangeCharSet@{tmp}\ISSprite.dll stdcall delayload';
//3
function spFntFreeFont(lpFont: Longword): Boolean;
  external 'spFntFreeFont@{tmp}\ISSprite.dll stdcall delayload';
//4
function spFntRegisterFont(lpFilename: PAnsiChar): Boolean;
  external 'spFntRegisterFont@{tmp}\ISSprite.dll stdcall delayload';
//5
function spFntUnregisterFont(lpFilename: PAnsiChar): Boolean;
  external 'spFntUnregisterFont@{tmp}\ISSprite.dll stdcall delayload';
//6
function spFntRegisterMemFont(lpBuffer: Longint; BuffLength: Longint): Longword;
  external 'spFntRegisterMemFont@{tmp}\ISSprite.dll stdcall delayload';
//7
function spFntUnregisterMemFont(hFont: Longword): Boolean;
  external 'spFntUnregisterMemFont@{tmp}\ISSprite.dll stdcall delayload';


//-- ###################################################################################### -- //

//1
function spBtnCreateButton(hParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'spBtnCreateButton@{tmp}\ISSprite.dll stdcall delayload';
//2
function spBtnCreateButtonFromBuffer(hParent: HWND; lpBuffer: Longint; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'spBtnCreateButtonFromBuffer@{tmp}\ISSprite.dll stdcall delayload';
//3
function spBtnCreateButtonFromResourceLibrary(hParent: HWND; lpResLibrary: PAnsiChar; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'spBtnCreateButtonFromResourceLibrary@{tmp}\ISSprite.dll stdcall delayload';
//4
function spBtnCreateButtonFromResourceName(hParent: HWND; lpInstance: longword; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'spBtnCreateButtonFromResourceName@{tmp}\ISSprite.dll stdcall delayload';
//5
function spBtnGetParent(hBtn: HWND): HWND;
  external 'spBtnGetParent@{tmp}\ISSprite.dll stdcall delayload';
//6
procedure spBtnRefresh(hBtn: HWND);
  external 'spBtnRefresh@{tmp}\ISSprite.dll stdcall delayload';
//7
procedure spBtnSetCursor(hBtn: HWND; lpCursor: Longword);
  external 'spBtnSetCursor@{tmp}\ISSprite.dll stdcall delayload';
//8
function spBtnGetSysCursor(lpCursorID: Integer): Longword;
  external 'spBtnGetSysCursor@{tmp}\ISSprite.dll stdcall delayload';
//9
procedure spBtnSetFont(hBtn: HWND; lpFont: Longword);
  external 'spBtnSetFont@{tmp}\ISSprite.dll stdcall delayload';
//10
procedure spBtnSetFontColor(hBtn: HWND; lpColorDefault, lpColorHover, lpColorPressed, lpColorDisabled: Cardinal);
  external 'spBtnSetFontColor@{tmp}\ISSprite.dll stdcall delayload';
//11
procedure spBtnSetText(hBtn: HWND; lpText: PAnsiChar);
  external 'spBtnSetText@{tmp}\ISSprite.dll stdcall delayload';
//12
procedure spBtnSetTextAlignment(hBtn: HWND; lpAlign: DWORD);
  external 'spBtnSetTextAlignment@{tmp}\ISSprite.dll stdcall delayload';
//13
procedure spBtnSetTextIndent(hBtn: HWND; iLeft, iTop: Integer);
  external 'spBtnSetTextIndent@{tmp}\ISSprite.dll stdcall delayload';
//14
procedure spBtnSetTag(hBtn: HWND; lpText: PAnsiChar);
  external 'spBtnSetTag@{tmp}\ISSprite.dll stdcall delayload';
//15
procedure spBtnGetTag(hBtn: HWND; var lpText: PAnsiChar; lpBuffSize: Longint);
  external 'spBtnGetTag@{tmp}\ISSprite.dll stdcall delayload';
//16
procedure spBtnSetEvent(hBtn: HWND; lpEventID: Integer; lpEvent: TImgButtonEvent);
  external 'spBtnSetEvent@{tmp}\ISSprite.dll stdcall delayload';
//17
procedure spBtnSetPos(hBtn: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spBtnSetPos@{tmp}\ISSprite.dll stdcall delayload';
//18
procedure spBtnGetPos(hBtn: HWND; var lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spBtnGetPos@{tmp}\ISSprite.dll stdcall delayload';
//19
procedure spBtnSetEnabled(hBtn: HWND; lpEnabled: Boolean);
  external 'spBtnSetEnabled@{tmp}\ISSprite.dll stdcall delayload';
//20
function spBtnGetEnabled(hBtn: HWND): Boolean;
  external 'spBtnGetEnabled@{tmp}\ISSprite.dll stdcall delayload';
//21
procedure spBtnSetVisible(hBtn: HWND; lpVisible: Boolean);
  external 'spBtnSetVisible@{tmp}\ISSprite.dll stdcall delayload';
//22
function spBtnGetVisible(hBtn: HWND): Boolean;
  external 'spBtnGetVisible@{tmp}\ISSprite.dll stdcall delayload';
//23
procedure spBtnSetChecked(hBtn: HWND; lpChecked: Byte);
  external 'spBtnSetChecked@{tmp}\ISSprite.dll stdcall delayload';
//24
function spBtnGetChecked(hBtn: HWND): Byte;
  external 'spBtnGetChecked@{tmp}\ISSprite.dll stdcall delayload';
//25
procedure spBtnSetTransparent(hBtn: HWND; lpAlpha: Byte);
  external 'spBtnSetTransparent@{tmp}\ISSprite.dll stdcall delayload';
//26
function spBtnGetTransparent(hBtn: HWND): Byte;
  external 'spBtnGetTransparent@{tmp}\ISSprite.dll stdcall delayload';

//-- ###################################################################################### -- //


procedure MoveSpriteHor(Img: Longword; Step, MinLeft, MaxLeft: Integer);
var
  x, y, x2, y2, ox, oy, px, py, px2, py2: Integer;
begin
  spImgGetPos(Img, x, y, x2, y2);
  spImgGetDimensions(Img, ox, oy);
  spImgGetVisiblePart(Img, px, py, px2, py2);
  x:=x+Step;
  if x2=0 then if Step<0 then x:= MaxLeft else x:= MinLeft;

  if Step>0 then begin
    if ((x+x2)>MaxLeft)and(x2>0) then begin
      px2:=px2-Step;
    end;
    if (x=MinLeft)and(x2<ox) then begin
      px2:=px2+Step;
      px:=ox-px2;
      x:=x-Step
    end;
  end else begin
    if (x<MinLeft)and(x2>0) then begin
      px2:=px2+Step;
      px:=ox-px2;
      x:=x-Step;
    end;
    if (x+ox>=MaxLeft)and(x2<ox) then begin
      px:=0;
      px2:=px2-Step;
    end;
  end;
  x2:=px2;
  spImgSetVisiblepart(Img, px, py, px2, py2);
  spImgSetPos(Img, x, y, x2, y2);
end;

function IsButtonChecked(hBtn: HWND): Boolean;
begin
  Result:= (spBtnGetChecked(hBtn) > 0);
end;

#ifdef IS_ENHANCED
function ImgLoadFromBuffer(lpParent: HWND; lpFilename: String; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
var
  buffer: String;
  FileSize: Longint;
begin
  lpFilename:= ExtractFileName(lpFilename);
  FileSize:= ExtractTemporaryFileSize(lpFilename);
  SetLength(Buffer, FileSize);
  
  ExtractTemporaryFileToBuffer(lpFilename, CastStringToInteger(Buffer));
  Result:= spImgLoadImageFromBuffer(lpParent, CastStringToInteger(Buffer), FileSize, lpLeft, lpTop, lpWidth, lpHeight, lpStretch, lpIsBkg);
  SetLength(buffer, 0);
end;

function BtnLoadFromBuffer(hParent: HWND; lpFilename: String; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
var
  buffer: String;
  FileSize: Longint;
begin
  lpFilename:= ExtractFileName(lpFilename);
  FileSize:= ExtractTemporaryFileSize(lpFilename);
  SetLength(Buffer, FileSize);
  
  ExtractTemporaryFileToBuffer(lpFilename, CastStringToInteger(Buffer));
  Result:= spBtnCreateButtonFromBuffer(hParent, CastStringToInteger(Buffer), FileSize, lpLeft, lpTop, lpWidth, lpHeight, lpStyle, lpShadowSize);
  SetLength(buffer, 0);
end;

function FntRegisterMemFont(lpFilename: String): Longword;
var
  buffer: String;
  FileSize: Longint;
begin
  lpFilename := ExtractFileName(lpFilename);
  FileSize := ExtractTemporaryFileSize(lpFilename);
  SetLength(Buffer, FileSize);

  ExtractTemporaryFileToBuffer(lpFilename, CastStringToInteger(Buffer));
  Result := spFntRegisterMemFont(CastStringToInteger(Buffer), FileSize);
  //SetLength(Buffer, 0);         // ������ ����������� �����
end;
#endif