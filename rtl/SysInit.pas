unit SysInit;

{$H+,I-,R-,S-,O+,W-}
{$WARN SYMBOL_PLATFORM OFF}
	
interface

var
  ModuleIsLib: Boolean;    
  ModuleIsPackage: Boolean;   
//  ModuleIsCpp: Boolean;      
  TlsIndex: Integer = -1;   
  TlsLast: Byte;               
  HInstance: LongWord;    
  DllProc: TDLLProc;    
  DllProcEx: TDLLProcEx absolute DllProc;
  DataMark: Integer = 0;   
//  CoverageLibraryName: array [0..128] of char = '*';
{$IFDEF ELF}
  TypeImportsTable: array [0..0] of Pointer platform;
  _GLOBAL_OFFSET_TABLE_: ARRAY [0..2] OF Cardinal platform;
  (* _DYNAMIC: ARRAY [0..0] OF Elf32_Dyn; *)
{$IFDEF PC_MAPPED_EXCEPTIONS}
  TextStartAdj: Byte platform;       
  CodeSegSize: Byte platform;      
function GetTextStart : LongInt; platform;
{$ENDIF}
{$ENDIF}

const
  PtrToNil: Pointer = nil;

type
  PMemInfo = ^TMemInfo;
  TMemInfo = packed record
  BaseAddress: Pointer;
  AllocationBase: Pointer;
  AllocationProtect: Longint;
    RegionSize: Longint;
    State: Longint;
    Protect: Longint;
    Type_9 : Longint;
  end;

  PStartupInfo = ^TStartupInfo;
  TStartupInfo = record
    cb: Longint;
    lpReserved: Pointer;
    lpDesktop: Pointer;
    lpTitle: Pointer;
    dwX: Longint;
    dwY: Longint;
    dwXSize: Longint;
    dwYSize: Longint;
    dwXCountChars: Longint;
    dwYCountChars: Longint;
    dwFillAttribute: Longint;
    dwFlags: Longint;
    wShowWindow: Word;
    cbReserved2: Word;
    lpReserved2: ^Byte;
    hStdInput: Integer;
    hStdOutput: Integer;
    hStdError: Integer;
  end;

  TWin32FindData = packed record
    dwFileAttributes: Integer;
    ftCreationTime: Int64;
    ftLastAccessTime: Int64;
    ftLastWriteTime: Int64;
    nFileSizeHigh: Integer;
    nFileSizeLow: Integer;
    dwReserved0: Integer;
    dwReserved1: Integer;
    cFileName: array[0..259] of Char;
    cAlternateFileName: array[0..13] of Char;
  end;	

  DWORD = Integer;
  BOOL  = LongBool;

  TRTLCriticalSection = packed record
    DebugInfo: Pointer;
    LockCount: Longint;
    RecursionCount: Longint;
    OwningThread: Integer;
    LockSemaphore: Integer;
    Reserved: DWORD;
  end;
	
function _GetTls: Pointer;
procedure _InitLib;
procedure _InitExe(InitTable: Pointer);
function _InitPkg(Hinst: Integer; Reason: Integer; Resvd: Pointer): LongBool; stdcall;
procedure _PackageLoad(const Table: PackageInfo);
procedure _PackageUnload(const Table: PackageInfo);
procedure VclInit(isDLL, isPkg: Boolean; hInst: LongInt; isGui: Boolean); cdecl;
procedure VclExit; cdecl;


function FreeLibrary(ModuleHandle: Longint): LongBool; stdcall;
function GetModuleHandle(ModuleName: PChar): Integer; stdcall;
function LocalAlloc(flags, size: Integer): Pointer; stdcall;
function LocalFree(addr: Pointer): Pointer; stdcall;
function TlsAlloc: Integer; stdcall;
function TlsFree(TlsIndex: Integer): Boolean; stdcall;
function TlsGetValue(TlsIndex: Integer): Pointer; stdcall;
function TlsSetValue(TlsIndex: Integer; TlsValue: Pointer): Boolean; stdcall;
function CloseHandle(Handle: Integer): Integer; stdcall;	
function CreateFileA(lpFileName: PChar; dwDesiredAccess, dwShareMode: Integer; lpSecurityAttributes: Pointer; dwCreationDisposition, dwFlagsAndAttributes: Integer; hTemplateFile: Integer): Integer; stdcall;
function DeleteFileA(Filename: PChar): LongBool;  stdcall;
function GetFileType(hFile: Integer): Integer; stdcall;
procedure GetSystemTime; stdcall;
function GetFileSize(Handle: Integer; x: Integer): Integer; stdcall;
function GetStdHandle(nStdHandle: Integer): Integer; stdcall;
function MoveFileA(OldName, NewName: PChar): LongBool; stdcall;
procedure RaiseException; stdcall;  
function ReadFile(hFile: Integer; var Buffer; nNumberOfBytesToRead: Cardinal; var lpNumberOfBytesRead: Cardinal; lpOverlapped: Pointer): Integer; stdcall;
procedure RtlUnwind; stdcall;
function SetEndOfFile(Handle: Integer): LongBool; stdcall;	
function SetFilePointer(Handle, Distance: Integer; DistanceHigh: Pointer; MoveMethod: Integer): Integer; stdcall;	
procedure UnhandledExceptionFilter; stdcall;	
function WriteFile(hFile: Integer; const Buffer; nNumberOfBytesToWrite: Cardinal; var lpNumberOfBytesWritten: Cardinal; lpOverlapped: Pointer): Integer; stdcall;
function CharNext(lpsz: PChar): PChar; stdcall;
function CreateThread(SecurityAttributes: Pointer; StackSize: LongWord; ThreadFunc: TThreadFunc; Parameter: Pointer; CreationFlags: LongWord; var ThreadId: LongWord): Integer; stdcall;
procedure ExitThread(ExitCode: Integer); stdcall;
procedure ExitProcess(ExitCode: Integer); stdcall;
procedure MessageBox(Wnd: Integer; Text: PChar; Caption: PChar; Typ: Integer); stdcall;
function CreateDirectory(PathName: PChar; Attr: Integer): WordBool; stdcall;
function FindClose(FindFile: Integer): LongBool; stdcall;
function FindFirstFile(FileName: PChar; var FindFileData: TWIN32FindData): Integer; stdcall;
function GetCommandLine: PChar; stdcall;
function GetCurrentDirectory(BufSize: Integer; Buffer: PChar): Integer; stdcall;
function GetLastError: Integer; stdcall;
procedure SetLastError(ErrorCode: Integer); stdcall;
function GetLocaleInfo(Locale: Longint; LCType: Longint; lpLCData: PChar; cchData: Integer): Integer; stdcall;
function GetModuleFileName(Module: Integer; Filename: PChar; Size: Integer): Integer; stdcall;
function GetProcAddress(Module: Integer; ProcName: PChar): Pointer; stdcall;
procedure GetStartupInfo(var lpStartupInfo: TStartupInfo); stdcall;
function GetThreadLocale: Longint; stdcall;
function LoadLibraryEx(LibName: PChar; hFile: Longint; Flags: Longint): Longint; stdcall;
function VirtualAlloc(lpAddress: Pointer; dwSize, flAllocationType, flProtect: DWORD): Pointer; stdcall;
function VirtualFree(lpAddress: Pointer; dwSize, dwFreeType: DWORD): BOOL; stdcall;
procedure InitializeCriticalSection(var lpCriticalSection: TRTLCriticalSection); stdcall;
procedure EnterCriticalSection(var lpCriticalSection: TRTLCriticalSection); stdcall;
procedure LeaveCriticalSection(var lpCriticalSection: TRTLCriticalSection); stdcall;
procedure DeleteCriticalSection(var lpCriticalSection: TRTLCriticalSection); stdcall;


function LoadString(Instance: Longint; IDent: Integer; Buffer: PChar; Size: Integer): Integer; stdcall;
function lstrcpy(lpString1, lpString2: PChar): PChar; stdcall;
function lstrcpyn(lpString1, lpString2: PChar; iMaxLength: Integer): PChar; stdcall;
function strlen(lpString: PChar): Integer; stdcall;
function MultiByteToWideChar(CodePage, Flags: Integer; MBStr: PChar; MBCount: Integer; WCStr: PWideChar; WCCount: Integer): Integer; stdcall;
function RegCloseKey(hKey: Integer): Longint; stdcall;
function RegOpenKeyEx(hKey: LongWord; lpSubKey: PChar; ulOptions, samDesired: LongWord; var phkResult: LongWord): Longint; stdcall;
function RegQueryValueEx(hKey: LongWord; lpValueName: PChar; lpReserved: Pointer; lpType: Pointer; lpData: PChar; lpcbData: Pointer): Integer; stdcall;
function RemoveDirectory(PathName: PChar): WordBool; stdcall;
function SetCurrentDirectory(PathName: PChar): WordBool; stdcall;
function WideCharToMultiByte(CodePage, Flags: Integer; WCStr: PWideChar; WCCount: Integer; MBStr: PChar; MBCount: Integer; DefaultChar: PChar; UsedDefaultChar: Pointer): Integer; stdcall;
function VirtualQuery(lpAddress: Pointer; var lpBuffer: TMemInfo; dwLength: Longint): Longint; stdcall;
function SysAllocStringLen(P: PWideChar; Len: Integer): PWideChar; stdcall;
function SysReAllocStringLen(var S: WideString; P: PWideChar; Len: Integer): LongBool; stdcall;
function SysStringLen(const S: WideString): Integer; stdcall;
function InterlockedIncrement(var Addend: Integer): Integer; stdcall;
function InterlockedDecrement(var Addend: Integer): Integer; stdcall;
function GetCurrentThreadId: LongWord; stdcall;
function GetVersion: LongWord; stdcall;
function QueryPerformanceCounter(var lpPerformanceCount: Int64): LongBool; stdcall;
function GetTickCount: Cardinal; stdcall;

	
implementation

const
  tlsArray      = $2C; 
  LMEM_ZEROINIT = $40;

  DLL_PROCESS_DETACH = 0;
  DLL_PROCESS_ATTACH = 1;
  DLL_THREAD_ATTACH  = 2;
  DLL_THREAD_DETACH  = 3;

var
  tlsBuffer: Pointer;
  Module: TLibModule = (
    Next: nil;
    Instance: 0;
    CodeInstance: 0;
    DataInstance: 0;
    ResInstance: 0;
    Reserved: 0);

function AllocTlsBuffer(Size: Integer): Pointer;
begin
  Result := LocalAlloc(LMEM_ZEROINIT, Size);
end;

function GetTlsSize: Integer;
begin
  Result := Integer(@TlsLast);
end;

procedure       InitThreadTLS;
var
  p: Pointer;
  tlsSize: Integer;
begin
  tlsSize := GetTlsSize;
  if tlsSize = 0 then  Exit;
  if TlsIndex = -1 then  RunError(226);
  p := AllocTlsBuffer(tlsSize);
  if p = nil then
    RunError(226)
  else
    TlsSetValue(TlsIndex, p);
end;

procedure       InitProcessTLS;
begin
  if @TlsLast = nil then
    Exit;
  TlsIndex := TlsAlloc;
  InitThreadTLS;
  tlsBuffer := TlsGetValue(TlsIndex);
end;

procedure       ExitThreadTLS;
var
  p: Pointer;
begin
  if @TlsLast = nil then
    Exit;
  if TlsIndex <> -1 then begin
    p := TlsGetValue(TlsIndex);
    if p <> nil then
      LocalFree(p);
  end;
end;

procedure       ExitProcessTLS;
begin
  if @TlsLast = nil then
    Exit;
  ExitThreadTLS;
  if TlsIndex <> -1 then
    TlsFree(TlsIndex);
end;

function _GetTls: Pointer;
asm
        MOV     CL,ModuleIsLib
        MOV     EAX,TlsIndex
        TEST    CL,CL
        JNE     @@isDll
        MOV     EDX,FS:tlsArray
        MOV     EAX,[EDX+EAX*4]
        RET

@@initTls:
        CALL    InitThreadTLS
        MOV     EAX,TlsIndex
        PUSH    EAX
        CALL    TlsGetValue
        TEST    EAX,EAX
        JE      @@RTM32
        RET

@@RTM32:
        MOV     EAX, tlsBuffer
        RET

@@isDll:
        PUSH    EAX
        CALL    TlsGetValue
        TEST    EAX,EAX
        JE      @@initTls
end;

const
  TlsProc: array [DLL_PROCESS_DETACH..DLL_THREAD_DETACH] of procedure =
    (ExitProcessTLS,InitProcessTLS,InitThreadTLS,ExitThreadTLS);

{$IFDEF PC_MAPPED_EXCEPTIONS}
const
  UNWINDFI_TOPOFSTACK =   $BE00EF00;

function GetTextStart : LongInt;
asm
        CALL  @@label1
@@label1:
        POP   EAX
        SUB   EAX, 5 + offset TextStartAdj
end;

function GetTextEnd : LongInt;
asm
        CALL  GetTextStart
        ADD   EAX, offset CodeSegSize
end;
{$ENDIF}

procedure InitializeModule;
begin
  RegisterModule(@Module);
end;

procedure UninitializeModule;
begin
  UnregisterModule(@Module);
  if (Module.ResInstance <> Module.Instance) and (Module.ResInstance <> 0) then
    FreeLibrary(Module.ResInstance);
end;

procedure VclInit(isDLL, isPkg: Boolean; hInst: LongInt; isGui: Boolean); cdecl;
begin
  ModuleIsLib := isDLL;
  ModuleIsPackage := isPkg;
  IsLibrary := isDLL and not isPkg;
  HInstance := hInst;
  Module.Instance := hInst;
  Module.CodeInstance := 0;
  Module.DataInstance := 0;
//  ModuleIsCpp := True;
  InitializeModule;
  if not ModuleIsLib then
  begin
    Module.CodeInstance := FindHInstance(@VclInit);
    Module.DataInstance := FindHInstance(@DataMark);
//    CmdLine := GetCommandLine;
//    IsConsole := not isGui;
  end;
end;

procedure VclExit; cdecl;
var
  P: procedure;
begin
  if not ModuleIsLib then
    while ExitProc <> nil do
    begin
      @P := ExitProc;
      ExitProc := nil;
      P;
    end;
  UnInitializeModule;
end;

{$IFDEF PC_MAPPED_EXCEPTIONS}
procedure RegisterPCMap;
begin
  SysRegisterIPLookup(GetTextStart,
                      GetTextEnd,
                      Pointer(GetTextEnd),
                      LongWord(@_Global_Offset_Table_));
end;

procedure UnregisterPCMap;
begin
  SysUnregisterIPLookup(GetTextStart);
end;
{$ENDIF}

function _InitPkg(Hinst: Longint; Reason: Integer; Resvd: Pointer): Longbool; stdcall;
begin
  ModuleIsLib := True;
  ModuleIsPackage := True;
  Module.Instance := Hinst;
  Module.CodeInstance := 0;
  Module.DataInstance := 0;
  HInstance := Hinst;
  if @TlsLast <> nil then
    TlsProc[Reason];
  if Reason = DLL_PROCESS_ATTACH then
    InitializeModule
  else if Reason = DLL_PROCESS_DETACH then
    UninitializeModule;
  _InitPkg := True;
end;

procedure _PackageLoad(const Table: PackageInfo);
begin
  System._PackageLoad(Table, @Module);
end;

procedure _PackageUnload(const Table: PackageInfo);
begin
  System._PackageUnload(Table, @Module);
end;

procedure _InitLib;
asm
        { ->    EAX Inittable   }
        {       [EBP+8] Hinst   }
        {       [EBP+12] Reason }
        {       [EBP+16] Resvd  }

        MOV     EDX,offset Module
        CMP     dword ptr [EBP+12],DLL_PROCESS_ATTACH
        JNE     @@notInit

        PUSH    EAX
        PUSH    EDX
        MOV     ModuleIsLib,1
        MOV     ECX,[EBP+8]
        MOV     HInstance,ECX
        MOV     [EDX].TLibModule.Instance,ECX
        MOV     [EDX].TLibModule.CodeInstance,0
        MOV     [EDX].TLibModule.DataInstance,0
        CALL    InitializeModule
        POP     EDX
        POP     EAX

@@notInit:
        PUSH    DllProc
        MOV     ECX,offset TlsProc
        CALL    _StartLib
end;

procedure _InitExe(InitTable: Pointer);
begin
  TlsIndex := 0;
  HInstance := GetModuleHandle(nil);
  Module.Instance := HInstance;
  Module.CodeInstance := 0;
  Module.DataInstance := 0;
  InitializeModule;
  _StartExe(InitTable, @Module);
end;

const
  kernel = 'kernel32.dll';
  advapi32 = 'advapi32.dll';
  user = 'user32.dll';
  oleaut = 'oleaut32.dll';

function FreeLibrary; external kernel name 'FreeLibrary';
function GetModuleHandle; external kernel name 'GetModuleHandleA';
function LocalAlloc; external kernel name 'LocalAlloc';
function LocalFree; external kernel name 'LocalFree';
function TlsAlloc; external kernel name 'TlsAlloc';
function TlsFree; external kernel name 'TlsFree';
function TlsGetValue; external kernel name 'TlsGetValue';
function TlsSetValue; external kernel name 'TlsSetValue';
function CloseHandle; external kernel name 'CloseHandle';
function CreateFileA; external kernel name 'CreateFileA';
function DeleteFileA; external kernel name 'DeleteFileA';
function GetFileType; external kernel name 'GetFileType';
procedure GetSystemTime;  external kernel name 'GetSystemTime';
function GetFileSize; external kernel name 'GetFileSize';
function GetStdHandle; external kernel name 'GetStdHandle';
function MoveFileA; external kernel name 'MoveFileA';
procedure RaiseException; external kernel name 'RaiseException';
function ReadFile; external kernel name 'ReadFile';
procedure RtlUnwind; external kernel name 'RtlUnwind';
function SetEndOfFile; external kernel name 'SetEndOfFile';
function SetFilePointer; external kernel name 'SetFilePointer';
procedure UnhandledExceptionFilter; external kernel name 'UnhandledExceptionFilter';
function WriteFile; external kernel name 'WriteFile';
function CharNext; external user name 'CharNextA';
function CreateThread; external kernel name 'CreateThread';
procedure ExitThread; external kernel name 'ExitThread';
procedure ExitProcess; external kernel name 'ExitProcess';
function CreateDirectory; external kernel name 'CreateDirectoryA';
function FindClose; external kernel name 'FindClose';
function FindFirstFile; external kernel name 'FindFirstFileA';
function GetCommandLine; external kernel name 'GetCommandLineA';
function GetCurrentDirectory; external kernel name 'GetCurrentDirectoryA';
function GetLastError; external kernel name 'GetLastError';
procedure SetLastError; external kernel name 'SetLastError';
function GetLocaleInfo; external kernel name 'GetLocaleInfoA';
function GetModuleFileName; external kernel name 'GetModuleFileNameA';
function GetProcAddress; external kernel name 'GetProcAddress';
procedure GetStartupInfo; external kernel name 'GetStartupInfoA';
function GetThreadLocale; external kernel name 'GetThreadLocale';
function LoadLibraryEx; external kernel name 'LoadLibraryExA';
function lstrcpy; external kernel name 'lstrcpyA';
function lstrcpyn; external kernel name 'lstrcpynA';
function strlen; external kernel name 'lstrlenA';
function MultiByteToWideChar; external kernel name 'MultiByteToWideChar';
function RemoveDirectory; external kernel name 'RemoveDirectoryA';
function SetCurrentDirectory; external kernel name 'SetCurrentDirectoryA';
function WideCharToMultiByte; external kernel name 'WideCharToMultiByte';
function VirtualQuery; external kernel name 'VirtualQuery';
function InterlockedIncrement; external kernel name 'InterlockedIncrement';
function InterlockedDecrement; external kernel name 'InterlockedDecrement';
function GetCurrentThreadId; external kernel name 'GetCurrentThreadId';
function GetVersion: LongWord; external kernel name 'GetVersion';
function QueryPerformanceCounter; external kernel name 'QueryPerformanceCounter';
function GetTickCount; external kernel name 'GetTickCount';	
function VirtualAlloc; external kernel name 'VirtualAlloc';
function VirtualFree; external kernel name 'VirtualFree';
procedure InitializeCriticalSection; external kernel name 'InitializeCriticalSection';
procedure EnterCriticalSection; external kernel name 'EnterCriticalSection';
procedure LeaveCriticalSection; external kernel name 'LeaveCriticalSection';
procedure DeleteCriticalSection; external kernel name 'DeleteCriticalSection';
	

function LoadString; external user name 'LoadStringA';
procedure MessageBox; external user   name 'MessageBoxA';

	
	
function RegCloseKey; external advapi32 name 'RegCloseKey';
function RegOpenKeyEx; external advapi32 name 'RegOpenKeyExA';
function RegQueryValueEx; external advapi32 name 'RegQueryValueExA';


	
function SysAllocStringLen; external oleaut name 'SysAllocStringLen';
function SysReAllocStringLen; external oleaut name 'SysReAllocStringLen';
function SysStringLen; external oleaut name 'SysStringLen';

end.


