type
  TFormInfo = packed record
    lpHandle          : HWND;
    lpOldStyle        : Longint;
    lpOldProc         : Longint;
    lpTimer           : Longword;

    lpBkLayer         : HDC;
    lpBkLayerBmp      : HBITMAP;
    lpBkLayerOld      : HBITMAP;
    lpBkLayerBits     : Pointer;
    lpBkLayerGP       : GPGRAPHICS;
		
    lpLayer           : HDC;
    lpLayerBmp        : HBITMAP;
    lpLayerOld        : HBITMAP;
    lpLayerBits       : Pointer;
    lpLayerGP         : GPGRAPHICS;

    lpSurface         : HDC;
    lpSurfaceBmp      : HBITMAP;
    lpSurfaceOld      : HBITMAP;
    lpSurfaceBits     : Pointer;
    lpSurfaceGP       : GPGRAPHICS;

    lpBitsCount       : Longint;
    lpBlendOp         : Longword;
    lpRawData         : Pointer;

    lpSprites         : Pointer;
    lpSpritesCount    : Longint;
    lpSpritesBkCount  : Longint;
    lpSpritesFrCount  : Longint;

    lpTexts           : Pointer;
    lpTextsCount      : Longint;

    lpButtons         : Pointer;
    lpButtonsCount    : Longint;

    lpIsLayered       : Boolean;
    lpIsChild         : Boolean;
    lplwaAlpha        : Byte;
    lpCtlBrush        : HBRUSH;

    lpWidth           : Longint;
    lpHeight          : Longint;

    dummy1            : Byte;
    dummy             : array [0..56-1] of Byte;
  end;
	
  PFormInfo = ^TFormInfo;

const
  SP_GIFDRAW          = $0400;
  SP_FORMDRAW         = $0200;
  SP_BKDRAW           = $0100;
  SP_DRAW             = $0080;

  SP_ISSPRITE         = $0040;
  SP_STRETCH          = $0020;

  SP_IMGMOVED         = $0008;
  SP_NEEDPAINT        = $0004;
  SP_NEEDUPDATE       = $0002;

  SP_REPAINT          = SP_NEEDPAINT or SP_NEEDUPDATE;

  SP_HORINVERT        = $0001;
  SP_VERTINVERT       = $0002;

  MAX_SPRITES         = 256;

  HEAP_ZERO_MEMORY    = $00000008;
  HEAP_NO_SERIALIZE   = $00000001;

type
  TSprite = packed record
    spLeft            : Longint;
    spTop             : Longint;
    spWidth           : Longint;
    spHeight          : Longint;

    spOldLeft         : Longint;
    spOldTop          : Longint;
    spOldWidth        : Longint;
    spOldHeight       : Longint;

    spPartLeft        : Longint;
    spPartTop         : Longint;
    spPartWidth       : Longint;
    spPartHeight      : Longint;

    spImgWidth        : Longint;
    spImgHeight       : Longint;
    spMaxWidth        : Longint;
    spMaxHeight       : Longint;
	
    spIndex           : Longint;
    spMaxIndex        : Longint;
    spRotate          : Longint;
		spOldRotate       : Longint;
    spVisiblePie      : Longint;
    spPiePath         : GPPATH;

    spCutColor        : Integer;
    spMyImage         : GPBITMAP;
    spPaintImage      : GPBITMAP;
    splwaAlpha        : Byte;
		
    spShowImg         : Boolean;
    spFlags           : DWORD;
    spParent          : HWND;
    spParentFI        : PFormInfo;

    spImageAttribs    : GpImageAttributes;
    spGifInterval     : Word;
    spGifIdleTime     : Word;
    spGifCurFrame     : Word;
    spGifMaxFrames    : Word;
    spGifTimer        : Longword;
    spGifLastEvent    : Longword;
    spGifGUID         : PGUID;

    spDrawOrder       : Longword;

    dummy             : Word;
    dummy2,
    dummy3, dummy4,
    dummy5, dummy6,
    dummy7, dummy8,
    dummy9, dummy10,
    dummy11, dummy12,
    dummy13, dummy14,
    dummy15           : DWORD;
    dummys            : array [0..59] of Byte;
  end;
	PSprite = ^TSprite;

const
  MAX_TEXTS           = 256;

  SHD_STYLEGLOW       = $0080;
  SHD_STYLESHADOW     = $0040;

  SHD_GRADIENTVER     = $0020;
  SHD_GRADIENTHOR     = $0010;

  SHD_NEEDPAINT       = $0004;
  SHD_NEEDBUILD       = $0002;

  SHD_NEEDREPAINT     = SHD_NEEDPAINT or SHD_NEEDBUILD;

  CLEARTYPE_QUALITY   = 5;

  TEXT_INDENTPIX      = 2;
  TEXT_INDENTANIS     = 10;

type
  TShadowText = packed record
    shdLeft           : Longint;
    shdTop            : Longint;
    shdWidth          : Longint;
    shdHeight         : Longint;

    shdOldLeft        : Longint;
    shdOldTop         : Longint;
    shdOldWidth       : Longint;
    shdOldHeight      : Longint;

    shdShadowSize     : Byte;
    shdShadowIntense  : Byte;
    shdTextColor      : Cardinal;
    shdShadowColor    : Cardinal;

    shdCaption        : PAnsiChar;
    shdVisible        : Boolean;
    shdFont           : HFONT;
    shdFlags          : DWORD;
    shdRotate         : Smallint;

    shdNeedGradient   : Boolean;
    shdBeginColor     : Cardinal;
    shdEndColor       : Cardinal;
    shdCharExtra      : Longint;

    shdTextDC         : HDC;
    shdBits           : Pointer;
    shdTextBitmap     : HBITMAP;
    shdOldBitmap      : HBITMAP;
    shdImage          : GPBITMAP;

    shdParent         : HWND;
    shdParentFI       : PFormInfo;
    
    shdMouseMoved     : Boolean;
    shdMousePressed   : Boolean;

    shdCbSpace        : Pointer;
    shdMouseEnter     : TFarProc;
    shdMouseMove      : TFarProc;
    shdMouseLeave     : TFarProc;
    shdMouseClick     : TFarProc;
    shdMouseDown      : TFarProc;
    shdMouseUp        : TFarProc;

    shdTag            : PAnsiChar;
  end;
	
  PShadowText = ^TShadowText;
	
const
  btnClassName        : array [0..10] of AnsiChar = 'TImgButton'#0;
  ClassImgButton      : array [0..9] of AnsiChar = 'ImgButton'#0;
  ClassButton         : array [0..6] of AnsiChar = 'Button'#0;

  cmnMouseClickEvent  = 1;
  cmnMouseEnterEvent  = 2;
  cmnMouseLeaveEvent  = 3;
  cmnMouseMoveEvent   = 4;
  cmnMouseDownEvent   = 5;
  cmnMouseUpEvent     = 6;

  btnAlignDefault     = 1;
  btnAlignTopLeft     = 2;
  btnAlignBottomLeft  = 3;
  btnAlignTopRight    = 4;
  btnAlignBottomRight = 5;
  btnAlignTopCenter   = 6;
  btnAlignBottomCenter= 7;
  btnAlignCenter      = 8;

  btnStateDefault     = 1;
  btnStateHovered     = 2;
  btnStatePressed     = 3;
  btnStateDisabled    = 4;

  BTN_PANEL           = $10;
  BTN_BUTTON          = $20;
  BTN_CHECKBOX        = $40;
  BTN_CHECKBOXEX      = $80;
  BTN_TAB             = $100;
//BTN_ANIMATE         = $200;

  MAX_BUTTONS         = 128;

type
  TCommonMouseEvent = procedure(hWnd: HWND; nEventID: Word); stdcall;

  TImgButton = packed record
    btnLeft           : Longint;
    btnTop            : Longint;
    btnWidth          : Longint;
    btnHeight         : Longint;

    btnImgWidth       : Longint;
    btnImgHeight      : Longint;
    btnImgStateH      : Longint;
    btnShadowSize     : Longint;

    btnCbSpace        : Pointer;
    btnMouseEnter     : TFarProc;
    btnMouseMove      : TFarProc;
    btnMouseLeave     : TFarProc;
    btnMouseClick     : TFarProc;
    btnMouseDown      : TFarProc;
    btnMouseUp        : TFarProc;

    btnCheckState     : Byte;
    btnCurImageState  : Byte;
    btnImgStateCount  : Byte;

    btnRebuild        : Boolean;
    btnVisible        : Boolean;
    btnEnabled        : Boolean;

    btnMouseMoved     : Boolean;
    btnMousePressed   : Boolean;

    btnHandle         : HWND;
    btnParent         : HWND;
    btnParentFI       : PFormInfo;

    btnImage          : GPBITMAP;
    btnCacheBmp       : HBITMAP;
    btnCacheOld       : HBITMAP;
    btnCacheBits      : Pointer;
    btnCacheDC        : HDC;

    btnSurfaceBmp     : HBITMAP;
    btnSurfaceOld     : HBITMAP;
    btnSurfaceBits    : Pointer;
    btnSurface        : HDC;

    btnBitsCount      : Longword;

    btnStyle          : DWORD;
    btnCursor         : HCURSOR;
    btnOldCursor      : HCURSOR;
    btnTransparent    : Byte;

    btnText           : PAnsiChar;
    btnTextAlign      : DWORD;
    btnFont           : HFONT;

    btnTag            : PAnsiChar;

    btnTextILeft      : Integer;
    btnTextITop       : Integer;
    btnAddWidth       : Integer;

    btnTextColorE     : Cardinal;
    btnTextColorH     : Cardinal;
    btnTextColorP     : Cardinal;
    btnTextColorD     : Cardinal;

    btnAspectRatio    : Double;

    btnDrawOrder      : Longword;

    dummy1,  dummy2,
    dummy3            : Byte;

    dummy13, dummy14,
    dummy15, dummy16,
    dummy17, dummy18,
    dummy19, dummy20,
    dummy21, dummy22,
    dummy23, dummy24,
    dummy25, dummy26,
    dummy27, dummy28  : Longword;
  end;
	
  PImgButton = ^TImgButton;

  TImageForm = packed record
    lpHandle          : HWND;
    lpPos             : TPoint;
    lpSize            : TSize;
    lpRect            : TRect;
    lpHDC             : HDC;
    lpGraphics        : GPGRAPHICS;
    lpControlDC       : HDC;

    dummy2,
    dummy3, dummy4,
    dummy5            : Longword;
  end;
  PImageForm = ^TImageForm;

  TArrayOfPoints  = array of TGPPOINT;

  TPointQuad      = array [0..3] of Longint;
  PPointQuad      = ^TPointQuad;

const
  MAX_INFO              = 64;
  SP_PAINTTIMER         = $0746;

  WM_USER               = $0400;

  WM_DESTROY            = $0002;
  WM_SIZE               = $0005;
  WM_SETFOCUS           = $0007;
  WM_KILLFOCUS          = $0008;
  WM_ENABLE             = $000A;
  WM_PAINT              = $000F;
  WM_SHOWWINDOW         = $0018;
	WM_CHANGEUISTATE      = $0127;
	WM_KEYDOWN						= $0100; {my_add}
  WM_ERASEBKGND         = $0014;
  WM_MOUSEMOVE          = $0200;
  WM_MOUSELEAVE         = $02A3;
  WM_MOUSEENTER         = WM_USER + 32;
  WM_MOUSENELEAVE       = WM_USER + 33;
  WM_MOUSECLICK         = WM_USER + 34;
  WM_DISABLE            = WM_USER + 35;
  WM_UPDATEBUTTON       = WM_USER + 36;
  WM_LBUTTONDOWN        = $0201;
  WM_LBUTTONUP          = $0202;
  WM_NCPAINT            = $0085;
  WM_WINDOWPOSCHANGED   = $0047;
  WM_CTLCOLOR           = $0019;
  WM_CTLCOLOREDIT       = $0133;
  WM_CTLCOLORLISTBOX    = $0134;
  WM_CTLCOLORBTN        = $0135;
  WM_CTLCOLORSTATIC     = $0138;
  WM_TIMER              = $0113;
  WM_PRINT              = 791;

  Gray: TColorMatrix  = ((1.0, 0, 0, 0, 0),
                         (0, 1.0, 0, 0, 0),
                         (0, 0, 1.0, 0, 0),
                         (0, 0, 0, 1.0, 0),
                         (0, 0, 0, 0, 1.0));												 

function spGetFormInfo(hWnd: HWND): PFormInfo;
var
  i: Longint;
  FI: PFormInfo;
begin
  Result:= nil;
  if (not ISSpriteMain.ISSpriteInit)or(ISSpriteMain.InfoCount=MAX_INFO) then Exit;
  i:= MAX_INFO-1;
  FI:= PFormInfo(Longword(ISSpriteMain.InfoPoint) + Longword(i*SizeOf(TFormInfo)));
  repeat
    if FI^.lpHandle=hWnd then begin
      Result:= FI;
      Exit;
    end;
    Dec(FI);
    Dec(i);
  until i < ISSpriteMain.InfoCount;
end;

function spIsValidSprite(img: Longword): PSprite;
var
  k: Longint;
  FI: PFormInfo;
  Min, Max: Longword;
begin
  Result:= nil;
  if (not ISSpriteMain.ISSpriteInit)or(ISSpriteMain.InfoCount=MAX_INFO) then Exit;
  k:= MAX_INFO-1;
  FI:= PFormInfo(Longword(ISSpriteMain.InfoPoint) + Longword(k*SizeOf(TFormInfo)));
  repeat
    Min:= Longword(FI^.lpSprites) + Longword((SizeOf(TSprite)*FI^.lpSpritesCount));
    Max:= Longword(FI^.lpSprites) + Longword((SizeOf(TSprite)*MAX_SPRITES));
    if (Img>=Min)and(Img<Max) then begin
      Result:= PSprite(Img);
      Break;
    end;
    Dec(FI);
    Dec(k);
  until k < ISSpriteMain.InfoCount;
end;

function spIsValidText(hText: Longword): Pointer;
var
  k: Longint;
  FI: PFormInfo;
  Min, Max: Longword;
begin
  Result:= nil;
  if not ISSpriteMain.ISSpriteInit then Exit;
  k:= MAX_INFO-1;
  FI:= PFormInfo(Longword(ISSpriteMain.InfoPoint) + Longword(k*SizeOf(TFormInfo)));
  repeat
    Min:= Longword(FI^.lpTexts) + Longword(FI^.lpTextsCount*SizeOf(TShadowText));
    Max:= Longword(FI^.lpTexts) + Longword(MAX_TEXTS*SizeOf(TShadowText));
    if (hText>=Min)and(hText<Max) then begin
      Result:= PShadowText(hText);
      Break;
    end;
    Dec(FI);
    Dec(k);
  until k < ISSpriteMain.InfoCount;
end;

function spGetButton(btn: HWND): Pointer;
var
  i, k: Longint;
  FI: PFormInfo;
  IB: PImgButton;
begin
  Result:= nil;
  if not ISSpriteMain.ISSpriteInit then Exit;
  k:= MAX_INFO-1;
  FI:= PFormInfo(Longword(ISSpriteMain.InfoPoint) + Longword(k*SizeOf(TFormInfo)));
  repeat
    i:= MAX_BUTTONS-1;
    IB:= PImgButton(Longword(FI^.lpButtons) + Longword(i*SizeOf(TImgButton)));
    repeat
      if IB^.btnHandle=btn then begin
        Result:= IB;
        Exit;
      end;
      Dec(IB);
      Dec(i);
    until i < FI^.lpButtonsCount;
    Dec(FI);
    Dec(k);
  until k < ISSpriteMain.InfoCount;
end;

function MouseCmnCallback(Param1, Param2: Longint): Longint; stdcall;
asm
  push  ebx
  mov   eax,0
  mov   ebx,0
  mov   edx,Param1
  mov   ecx,Param2
  call  ebx
  pop   ebx
end;

function ResourceSwitch(S: PAnsiChar): PAnsiChar;
begin
  case AnsiIndexText(AnsiUppercase(S), ['RT_CURSOR', 'RT_BITMAP', 'RT_ICON', 'RT_MENU', 'RT_DIALOG',
  'RT_STRING', 'RT_FONTDIR', 'RT_FONT', 'RT_ACCELERATOR', 'RT_RCDATA', 'RT_MESSAGETABLE', 'RT_GROUP_CURSOR',
  'RT_GROUP_ICON', 'RT_VERSION', 'RT_DLGINCLUDE', 'RT_PLUGPLAY', 'RT_VXD', 'RT_ANICURSOR', 'RT_ANIICON']) of
    0: Result:= RT_CURSOR;
    1: Result:= RT_BITMAP;
    2: Result:= RT_ICON;
    3: Result:= RT_MENU;
    4: Result:= RT_DIALOG;
    5: Result:= RT_STRING;
    6: Result:= RT_FONTDIR;
    7: Result:= RT_FONT;
    8: Result:= RT_ACCELERATOR;
    9: Result:= RT_RCDATA;
    10: Result:= RT_MESSAGETABLE;
    11: Result:= RT_GROUP_CURSOR;
    12: Result:= RT_GROUP_ICON;
    13: Result:= RT_VERSION;
    14: Result:= RT_DLGINCLUDE;
    15: Result:= RT_PLUGPLAY;
    16: Result:= RT_VXD;
    17: Result:= RT_ANICURSOR;
    18: Result:= RT_ANIICON;
    else Result:= S;
  end;
end;

procedure spRotatePixel(X0, Y0: Integer; Angle: Double; var X, Y: Integer);
var
  l, a: Extended;
begin
  l:= sqrt(x0*x0+y0*y0);
  if X0=0 then begin
    if Y0<0 then
      a:= -PI/2
    else
      a:= PI/2;
  end else
    a:= ArcTan(Y0/X0);
  if X0<0 then
    a:=a+PI;
  X:= round(L*cos(A+Angle));
  Y:= round(L*sin(A+Angle));
end;

function minp(P: PPointQuad): Integer;
begin
  Result:= p^[0];
  if P^[1]<Result then Result:= P^[1];
  if P^[2]<Result then Result:= P^[2];
  if P^[3]<Result then Result:= P^[3];
end;

function maxp(P: PPointQuad): Integer;
begin
  Result:= p^[0];
  if P^[1]>Result then Result:= P^[1];
  if P^[2]>Result then Result:= P^[2];
  if P^[3]>Result then Result:= P^[3];
end;

procedure spSetRotateRect(r: PRect; P: PPointQuad; lpAngle: Integer);
var
  cx, cy: Integer;
  y, x, nb: TPointQuad;
  n: Extended;
begin
  if lpAngle<>0 then begin
    cx:= (P^[2] div 2);
    cy:= (P^[3] div 2);
    n:=lpAngle*PI/180;
    spRotatePixel(-cx, -cy, n, x[0], y[0]);
    spRotatePixel(cx, -cy, n, x[1], y[1]);
    spRotatePixel(-cx, cy, n, x[2], y[2]);
    spRotatePixel(cx, cy, n, x[3], y[3]);
    nb[0]:= minp(@x)+P^[0]+cx;
    nb[1]:= minp(@y)+P^[1]+cy;
    nb[2]:= maxp(@x)+P^[0]+cx;
    nb[3]:= maxp(@y)+P^[1]+cy;
    SetRectII(r, @nb);
  end else
    SetRectI(r, P);
end;

procedure spSetPoints(const aL, aT, aW, aH: Integer; Rotate: Integer; Points: TArrayOfPoints);
var
  cx, cy, x, y: Integer;
  n: Extended;
begin
  cx:= aW div 2;
  cy:= aH div 2;
  n:= Rotate*PI/180;
  spRotatePixel(-cx, -cy, n, x, y);
  Points[0].X:= aL+x+cx;
  Points[0].Y:= aT+y+cy;
  spRotatePixel(cx, -cy, n, x, y);
  Points[1].X:= aL+x+cx;
  Points[1].Y:= aT+y+cy;
  spRotatePixel(-cx, cy, n, x, y);
  Points[2].X:= aL+x+cx;
  Points[2].Y:= aT+y+cy;
end;

procedure spDrawImage(GP: GPGRAPHICS; SP: PSprite);
var
  tmp: array [0..2] of TGPPoint;
begin
  with SP^ do begin
    if spMyImage <> nil then begin

      if spVisiblePie <> -1 then GdipSetClipPath(GP, spPiePath, CombineModeReplace);

      if spRotate <> 0 then begin
        spSetPoints(spLeft, spTop, spWidth, spHeight, spRotate, @tmp);
        GdipDrawImagePointsRectI(GP, spMyImage, PGPPOINT(@tmp), 3, spPartLeft, spPartTop, spPartWidth, spPartHeight, UnitPixel, spImageAttribs, nil, nil);
      end else
				GdipDrawImageRectRectI(GP, spMyImage, spLeft, spTop, spWidth, spHeight, spPartLeft, spPartTop, spPartWidth, spPartHeight, UnitPixel, spImageAttribs, nil, nil);

      if spVisiblePie <> -1 then GdipResetClip(GP);
    end;
  end;
end;

procedure spPaintShadowText(hWnd: HWND; GP: GPGRAPHICS);
var
  FI: PFormInfo;
  i, sz: Integer;
  tmp: array [0..2] of TGPPoint;
begin
  FI:= spGetFormInfo(hWnd);
  if (FI=nil)or(FI^.lpTextsCount=MAX_TEXTS) then Exit;
  i:= MAX_TEXTS-1;
  repeat
    with PShadowText(Longword(FI^.lpTexts) + Longword(i*SizeOf(TShadowText)))^ do begin
      if (shdVisible)and(shdImage<>nil) then begin
        sz:= (shdShadowSize+TEXT_INDENTPIX)*2;
        if shdRotate=0 then
          GdipDrawImageRectI(GP, shdImage, shdLeft, shdTop, shdWidth+sz, shdHeight+sz)
        else begin
          spSetPoints(shdLeft, shdTop, shdWidth+sz, shdHeight+sz, shdRotate, @tmp);
          GdipDrawImagePointsRectI(GP, shdImage, PGPPOINT(@tmp), 3, 0, 0, shdWidth+sz, shdHeight+sz, UnitPixel, nil, nil, nil);
        end;
      end;
    end;
    Dec(i);
  until i < FI^.lpTextsCount;
end;

procedure BuildButton(IB: PImgButton);
var
	r: TRect;
	n: Integer;
	oldFont: HFONT;
	newColor: Cardinal;
  memGP: GPGRAPHICS;
  tmpDC, tmpDC2: HDC;
	bmiInfo: TBitmapInfo;
	lpBits1, lpBits2: Pointer;
  tmpBmp1, oldBmp1, tmpBmp2, oldBmp2: HBITMAP;
begin
	if (IB^.btnWidth = 0) or (IB^.btnHeight = 0) then Exit;
  with IB^ do begin
    GdipCreateFromHDC(btnCacheDC, memGP);
    ClearBits(btnCacheBits, btnBitsCount*btnImgStateCount);

    for n:=0 to btnImgStateCount-1 do
      GdipDrawImageRectRectI(memGP, btnImage, 0, n*btnHeight, btnAddWidth, btnHeight, btnShadowSize, n*btnImgStateH+btnShadowSize, btnImgWidth-(btnShadowSize*2), btnImgStateH-(btnShadowSize*2), UnitPixel, nil, nil, nil);
    if (btnText<>nil) then begin
      tmpDC:= CreateCompatibleDC(btnCacheDC);
      tmpDC2:= CreateCompatibleDC(btnCacheDC);
      FillBmiInfo(btnWidth, btnHeight, @bmiInfo);
      tmpBmp1:= CreateDIBSection(btnCacheDC, bmiInfo, DIB_RGB_COLORS, lpBits1, 0, 0);
      tmpBmp2:= CreateDIBSection(btnCacheDC, bmiInfo, DIB_RGB_COLORS, lpBits2, 0, 0);
      oldBmp1:= SelectObject(tmpDC, tmpBmp1);
      oldBmp2:= SelectObject(tmpDC2, tmpBmp2);
      ClearBits(lpBits1, btnBitsCount);
      oldFont:= SelectObject(tmpDC, btnFont);
      SetBkMode(tmpDC, TRANSPARENT);
      SetTextColor(tmpDC, $FFFFFF);
      SetRect(r, btnTextILeft, btnTextITop, btnWidth, btnHeight);
      DrawText(tmpDC, btnText, Length(btnText), r, btnTextAlign);
      for n:=0 to btnImgStateCount-1 do begin
        ClearBits(lpBits2, btnBitsCount);
        case (n mod 4) of
          0: newColor:=btnTextColorE;
          1: newColor:=btnTextColorH;
          2: newColor:=btnTextColorP;
          3: newColor:=btnTextColorD;
          else
            newColor:= $FFFFFF;
        end;
        MaskBits(lpBits1, lpBits2, newColor, btnBitsCount);
        BlendBits(lpBits2, PAnsiChar(btnCacheBits)+(DWORD(n)*btnBitsCount) shl 2, btnBitsCount);
      end;
      SelectObject(tmpDC, oldFont);
      SelectObject(tmpDC, oldBmp1);
      SelectObject(tmpDC2, oldBmp2);
      DeleteObject(tmpDC);
      DeleteObject(tmpDC2);
      DeleteObject(tmpBmp1);
      DeleteObject(tmpBmp2);
    end;
    GdipDeleteGraphics(memGP);
  end;
end;

function spBtnWndProc(h: HWND; Msg, wParam, lParam: Longword): Longword; stdcall;
var
  PFI: PFormInfo;
  IB: PimgButton;
  ps: TPaintStruct;
  trme: tagTRACKMOUSEEVENT;
  DC, btnDC: HDC;
  s: Integer;
  cr: TPoint;
  rgb: PRGBQuad;
begin
  IB:= spGetButton(h);
  if IB=nil then begin
    Result:= DefWindowProc(h, msg, wParam, lParam);
    Exit;
  end;

  Result:= 0;
  PFI:= IB^.btnParentFI;
  case Msg of
    WM_ERASEBKGND: Result:= 1;
    WM_NCPAINT: Result:= 0;
    WM_SHOWWINDOW, WM_WINDOWPOSCHANGED: begin
      Result:= DefWindowProc(h, msg, wParam, lParam);
      InvalidateRect(h, nil, False);
      UpdateWindow(h);
    end;
    WM_SETFOCUS, WM_KILLFOCUS: begin
      Result:= DefWindowProc(h, msg, wParam, lParam);
      InvalidateRect(h, nil, False);
      UpdateWindow(h);
    end;
    WM_ENABLE: begin
      if (wParam = 0) then begin
        SendMessage(h, WM_DISABLE, 0, 0);
        Exit;
      end;
			
      if (IB^.btnEnabled) then Exit;

      IB^.btnEnabled:= True;
      SendMessage(h, WM_UPDATEBUTTON, btnStateDefault, 0);
    end;
    WM_DISABLE: begin
      if (not IB^.btnEnabled) then Exit;

      IB^.btnEnabled:= False;
      SendMessage(h, WM_MOUSENELEAVE, 0, 0);
      SendMessage(h, WM_UPDATEBUTTON, btnStateDisabled, 0);
    end;
    WM_UPDATEBUTTON: begin
      s:= 1;
      if IB^.btnStyle > BTN_PANEL then
        s:= (IB^.btnCheckState shl 2) + wParam;
      if IB^.btnCurImageState <> s then begin
        IB^.btnCurImageState:= s;
        InvalidateRect(IB^.btnHandle, nil, False);
        UpdateWindow(IB^.btnHandle);
      end;
    end;
    WM_PAINT: begin
      DC:= BeginPaint(h, ps);
      if wParam<>0 then btnDC:= HDC(wParam) else
        btnDC:= DC;
      with IB^ do begin
        ClearBits(btnSurfaceBits, btnBitsCount);
        if lParam<>1 then
          BitBlt(btnSurface, 0, 0, btnWidth, btnHeight, PFI^.lpSurface, btnLeft, btnTop, SRCCOPY);
        if btnTransparent=$FF then
          BlendBits(PAnsiChar(btnCacheBits)+(DWORD(btnCurImageState-1)*btnBitsCount) shl 2, btnSurfaceBits, btnBitsCount)
        else
          BlendBitsII(PAnsiChar(btnCacheBits)+(DWORD(btnCurImageState-1)*btnBitsCount) shl 2, btnSurfaceBits, btnBitsCount, btnTransparent);

        if (PFI^.lpSpritesFrCount > 0) then
          BlendRect(PFI^.lpLayerBits, btnLeft, btnTop, PFI^.lpWidth, PFI^.lpHeight, btnSurfaceBits, btnWidth, btnHeight);

        BitBlt(btnDC, 0, 0, btnWidth, btnHeight, btnSurface, 0, 0, SRCCOPY);
      end;
      EndPaint(h, ps);
    end;
    WM_MOUSENELEAVE: begin
      IB^.btnMouseMoved:= False;
      IB^.btnMousePressed:= False;

      if IB^.btnOldCursor <> INVALID_HANDLE_VALUE then begin
        SetCursor(IB^.btnOldCursor);
        IB^.btnOldCursor:= INVALID_HANDLE_VALUE;
      end;

      trme.cbSize:= SizeOf(trme);
      trme.dwFlags:= TME_LEAVE or TME_CANCEL;
      trme.hwndTrack:= h;
      TrackMouseEvent(trme);
    end;
    WM_MOUSEENTER: begin
      if (IB^.btnMouseMoved) then Exit;
      IB^.btnMouseMoved:= True;

      if IB^.btnCursor <> INVALID_HANDLE_VALUE then begin
        IB^.btnOldCursor:= GetCursor();
        SetCursor(IB^.btnCursor);
      end;

      trme.cbSize:= SizeOf(trme);
      trme.dwFlags:= TME_LEAVE;
      trme.hwndTrack:= h;
      TrackMouseEvent(trme);

      SendMessage(h, WM_UPDATEBUTTON, btnStateHovered, 0);
      if IB^.btnMouseEnter <> nil then
				TCommonMouseEvent(IB^.btnMouseEnter)(h, cmnMouseEnterEvent);
    end;
    WM_MOUSEMOVE: begin
      if (not IB^.btnEnabled) then Exit;

      if (PFI^.lpSpritesFrCount > 0) then begin
        GetCursorPos(cr);
        MapWindowPoints(0, PFI^.lpHandle, cr, 1);
        rgb:= Pointer(Longint(PFI^.lpLayerBits) + (cr.Y*PFI^.lpWidth + cr.X)*Sizeof(TRGBQuad));
        if (rgb^.rgbReserved > 10) then begin
          if (not IB^.btnMousePressed) then begin
            SendMessage(h, WM_MOUSELEAVE, 0, 0);
            Exit;
          end;
        end;
      end;

      if (not IB^.btnMouseMoved) then begin
        SendMessage(h, WM_MOUSEENTER, 0, 0);
      end else begin
        if IB^.btnMouseMove<>nil then
          TCommonMouseEvent(IB^.btnMouseMove)(h, cmnMouseMoveEvent);
      end;
    end;
    WM_MOUSELEAVE: begin
      if (not IB^.btnEnabled) then Exit;

      SendMessage(h, WM_MOUSENELEAVE, 0, 0);
      SendMessage(h, WM_UPDATEBUTTON, btnStateDefault, 0);

      if IB^.btnMouseLeave<>nil then
        TCommonMouseEvent(IB^.btnMouseLeave)(h, cmnMouseLeaveEvent);
    end;
    WM_LBUTTONDOWN: begin
      if (not IB^.btnEnabled) or (not IB^.btnMouseMoved) then Exit;

      if (wParam and MK_LBUTTON) <> 0 then begin
        SendMessage(h, WM_UPDATEBUTTON, btnStatePressed, 0);

        SetCapture(h);
        IB^.btnMousePressed:= True;

        if IB^.btnMouseDown<>nil then
          TCommonMouseEvent(IB^.btnMouseDown)(h, cmnMouseDownEvent);
      end;
    end;
    WM_LBUTTONUP: begin
      if (not IB^.btnEnabled) then Exit;

      if (PFI^.lpSpritesFrCount > 0) then begin
        GetCursorPos(cr);
        MapWindowPoints(0, PFI^.lpHandle, cr, 1);
        rgb:= Pointer(Longint(PFI^.lpLayerBits) + (cr.Y*PFI^.lpWidth + cr.X)*Sizeof(TRGBQuad));
        if (rgb^.rgbReserved > 10) then
          if (not IB^.btnMousePressed) then
            Exit;
      end;

      SendMessage(h, WM_UPDATEBUTTON, btnStateHovered, 0);
      ReleaseCapture();

      if (IB^.btnMousePressed) then begin
        IB^.btnMousePressed:= False;
        if IB^.btnStyle > BTN_BUTTON then begin
          if (IB^.btnCheckState = 2) then
            IB^.btnCheckState:= 1
          else
            IB^.btnCheckState:= Byte(not Boolean(IB^.btnCheckState));
          SendMessage(h, WM_UPDATEBUTTON, btnStateHovered, 0);
        end;

        if IB^.btnMouseClick <> nil then
          TCommonMouseEvent(IB^.btnMouseClick)(h, cmnMouseClickEvent);
      end;

      if IB^.btnMouseUp<>nil then
        TCommonMouseEvent(IB^.btnMouseUp)(h, cmnMouseUpEvent);
    end;
  else
    Result:= DefWindowProc(h, Msg, wParam, lParam);
  end;
end;

function spDrawChildWindows(hWnd: HWND; lParam: PImageForm): Boolean; stdcall;
var
  bmp, oldbmp: HBITMAP;
  r: TRect;
  cx, cy, i: Integer;
  tmp: array [0..31] of Char;
  lpMem: Pointer;
  Bits: PRGBQuad;
  bmiInfo: TBitmapInfo;
  lp: TImageForm;
Label
  skip;
begin
  if IsWindowVisible(hWnd)and(GetParent(hWnd) = PImageForm(lParam)^.lpHandle) then begin
    FillChar(tmp[0], 32, 0);
    RealGetWindowClass(hWnd, @tmp[0], 32);

    GetWindowRect(hWnd, r);
    MapWindowPoints(0, lParam^.lpHandle, r, 2);
    cx:= r.Right-r.Left;
    cy:= r.Bottom-r.Top;

    if (cx<=0)or(cy<=0) then goto skip;

    FillBmiInfo(cx, cy, @bmiInfo);
    bmp:= CreateDIBSection(lParam^.lpControlDC, bmiInfo, DIB_RGB_COLORS, lpMem, 0, 0);
    oldbmp:= SelectObject(lParam^.lpControlDC, bmp);
  //BitBlt(lParam^.lpControlDC, 0, 0, cx, cy, lParam^.lpHDC, r.Left, r.Top, SRCCOPY);
  //SendMessage(hWnd, WM_PAINT, lParam^.lpControlDC, 0);
    if (Pos(ClassImgButton, tmp) = 0) then begin
      PrintWindow(hWnd, lParam^.lpControlDC, 0);

      Bits:= lpMem;
      i:= cy*cx;
      if i<>0 then
      repeat
        Dec(i);
        if (Bits^.rgbReserved=0) then
          Bits^.rgbReserved:= $FF
        else if (Bits^.rgbReserved=1) then
          Bits^.rgbReserved:= 0;
        Inc(Bits);
      until i=0;
    end else
      SendMessage(hWnd, WM_PAINT, lParam.lpControlDC, 0);

    Move(lParam^, lp, SizeOf(TImageForm));
    lp.lpHandle:= hWnd;
    lp.lpHDC:= lParam^.lpControlDC;
    lp.lpControlDC:= CreateCompatibleDC(lp.lpHDC);
    EnumChildWindows(hWnd, @spDrawChildWindows, Longint(@lp));
    DeleteObject(lp.lpControlDC);

    BitBlt(lParam^.lpHDC, r.Left, r.Top, cx, cy, lParam^.lpControlDC, 0, 0, SRCCOPY);
    SelectObject(lParam^.lpControlDC, oldbmp);
    DeleteObject(bmp);
  end;

skip:
  Result:= True;
end;

procedure spUpdateLayeredWindow(hWnd: HWND; FI: PFormInfo);
var
  DC      : HDC;
  TIF     : TImageForm;
  i       : Longint;
  SP      : PSprite;
//qpc1, qpc2: TLargeInteger;
begin
//QueryPerformanceCounter(qpc1);
  DC:= GetDC(0);
  FillChar(TIF, SizeOf(TImageForm), 0);
  GetWindowRect(hWnd, TIF.lpRect);
  TIF.lpHandle              := FI^.lpHandle;
  TIF.lpSize.cx             := TIF.lpRect.Right-TIF.lpRect.Left;
  TIF.lpSize.cy             := TIF.lpRect.Bottom-TIF.lpRect.Top;
  TIF.lpHDC                 := FI^.lpSurface;
  TIF.lpControlDC           := CreateCompatibleDC(TIF.lpHDC);
  TIF.lpGraphics            := FI^.lpSurfaceGP;
  ClearBits(FI^.lpSurfaceBits, FI^.lpBitsCount);
  
  BlendBits(FI^.lpBkLayerBits, FI^.lpSurfaceBits, FI^.lpBitsCount);

  i:= MAX_SPRITES-1;
  repeat
    SP:= PSprite(Longword(FI^.lpSprites) + Longword(i*SizeOf(TSprite)));
    if (SP^.spFlags and SP_GIFDRAW) <> 0 then
      if SP^.spShowImg then spDrawImage(FI^.lpSurfaceGP, SP); {my_add}
    Dec(i);
  until i < FI^.lpSpritesCount;

  EnumChildWindows(hWnd, @spDrawChildWindows, Longint(@TIF));
  spPaintShadowText(hWnd, TIF.lpGraphics);
  BlendBits(FI^.lpLayerBits, FI^.lpSurfaceBits, FI^.lpBitsCount);

  UpdateLayeredWindow(hWnd, DC, nil, @TIF.lpSize, TIF.lpHDC, @TIF.lpPos, $00000000, @FI^.lpBlendOp, ULW_ALPHA);
  DeleteObject(TIF.lpControlDC);
  ReleaseDC(0, DC);

//QueryPerformanceCounter(qpc2);
//ShowMessage('spUpdateLayeredWindow time: '+intToStr(qpc2-qpc1));
end;

procedure spGifTimerProc(h: HWND; Msg: Longword; idEvent: Longword; dwTime: Longword); stdcall;
var
  SP: PSprite;
  r: TRect;
begin
  SP:= PSprite(idEvent);
  
  with SP^ do begin 
    if (not spShowImg) or ((spGifCurFrame = 0) and ((dwTime - spGifLastEvent) < spGifIdleTime)) then Exit;

    spGifLastEvent:= dwTime;
    Inc(spGifCurFrame);
    if spGifCurFrame = spGifMaxFrames then spGifCurFrame:= 0;
    GdipImageSelectActiveFrame(spMyImage, spGifGUID, spGifCurFrame);
    SetRectI(@r, @SP^.spLeft);
    InvalidateRect(h, @r, False);
    UpdateWindow(h);
  end;
end;

function spShdMouseWrapper(ST: PShadowText; Msg: Longword; wParam, lParam: Longword): Longword;
var
  w: Boolean;
begin
  Result:= 0;
  if ST = nil then Exit;
  
  case Msg of
    WM_MOUSEMOVE: begin
      if not ST^.shdMouseMoved then begin
        spShdMouseWrapper(ST, WM_MOUSEENTER, 0, 0);
        Exit;
      end;
      if (ST^.shdMouseMove <> nil) then TCommonMouseEvent(ST^.shdMouseMove)(Longword(ST), cmnMouseMoveEvent);
      Result:= WM_MOUSEMOVE;
    end;
    WM_MOUSEENTER: begin
      if ST^.shdMouseMoved then Exit;
      ST^.shdMouseMoved:= True;
      if (ST^.shdMouseEnter <> nil) then TCommonMouseEvent(ST^.shdMouseEnter)(Longword(ST), cmnMouseEnterEvent);
      Result:= WM_MOUSEENTER;
    end;
    WM_MOUSELEAVE: begin
      if not ST^.shdMouseMoved then Exit;
      ST^.shdMousePressed:= False;
      ST^.shdMouseMoved:= False;
      if (ST^.shdMouseLeave <> nil) then TCommonMouseEvent(ST^.shdMouseLeave)(Longword(ST), cmnMouseLeaveEvent);
      Result:= WM_MOUSELEAVE;
    end;
    WM_LBUTTONUP: begin
      if not ST^.shdMouseMoved then Exit;
      w:= ST^.shdMousePressed;
      ST^.shdMousePressed:= False;
      if w then begin
        spShdMouseWrapper(ST, WM_MOUSECLICK, 0, 0);
        Exit;
      end;
      if (ST^.shdMouseUp <> nil) then TCommonMouseEvent(ST^.shdMouseUp)(Longword(ST), cmnMouseUpEvent);
      Result:= WM_LBUTTONUP;
    end;
    WM_LBUTTONDOWN: begin
      if not ST^.shdMouseMoved then Exit;
      ST^.shdMousePressed:= True;
      if ST^.shdMouseDown <> nil then TCommonMouseEvent(ST^.shdMouseDown)(Longword(ST), cmnMouseDownEvent);
      Result:= WM_LBUTTONDOWN;
    end;
    WM_MOUSECLICK: begin
      if not ST^.shdMouseMoved then Exit;
      if ST^.shdMouseClick <> nil then TCommonMouseEvent(ST^.shdMouseClick)(Longword(ST), cmnMouseClickEvent);
      Result:= WM_MOUSECLICK;
    end;
    else Exit;
  end;
end;

function spShdMouseCaller(FI: PFormInfo; Msg: Longword; wParam, lParam: Longword): Longint;
var
  i: Longword;
  b: Boolean;
  r: TRect;
  p: TPoint;
  ST: PShadowText;
begin
  Result:= -1;
 
  if (FI = nil) or (FI^.lpTextsCount = MAX_TEXTS) then Exit;

  b:= False;
  i:= FI^.lpTextsCount;
  while (i < MAX_TEXTS) do begin
    ST:= PShadowText(Longword(FI^.lpTexts) + i*SizeOf(TShadowText));
    if (ST^.shdVisible) then begin
      GetCursorPos(p);
      MapWindowPoints(0, FI^.lpHandle, p, 1);
      SetRectI(@r, @ST^.shdLeft);
      if (InRect(p, r) and (not b)) then begin
        Result:= spShdMouseWrapper(ST, Msg, wParam, lParam);
        if (Result = WM_MOUSEENTER) then b:= False;
      end else begin
        spShdMouseWrapper(ST, WM_MOUSELEAVE, 0, 0);
      end;
    end;
    Inc(i);
  end;
end;

function spWndPaint(h: HWND; Msg, wParam, lParam: Longword): Longword; stdcall;
var
  ps		  : TPaintStruct;
  r, r1		  : TRect;
  DC, hMemDC  : HDC;
  FI          : PFormInfo;
  mBmp, oBmp  : HBITMAP;
  bmiInfo     : TBitmapInfo;
  mBits       : Pointer;
  i           : Integer;
  SP          : PSprite;
  rgb         : PRGBQuad;
begin
  FI:= spGetFormInfo(h);
  if (FI=nil) then begin
    Result:=CallWindowProc(Pointer(GetWindowLong(h,GWL_WNDPROC)),h,Msg,wParam,lParam);
    Exit;
  end;
	
  case Msg of
		WM_ERASEBKGND: Result:= 1; {my_add}
		WM_CHANGEUISTATE: Result:= 0; {my_add}    
    WM_MOUSEMOVE, WM_MOUSEENTER, WM_MOUSELEAVE,
    WM_LBUTTONUP, WM_LBUTTONDOWN: begin
      Result:= 0;
      if (spShdMouseCaller(FI, Msg, wParam, lParam) = -1) then
      Result:= CallWindowProc(Pointer(FI^.lpOldProc), h, msg, wParam, lParam);
    end;
    WM_CTLCOLOREDIT, WM_CTLCOLORLISTBOX,
    WM_CTLCOLORBTN, WM_CTLCOLORSTATIC: begin
      Result:= CallWindowProc(Pointer(FI^.lpOldProc), h, msg, wParam, lParam);
      if ISSpriteMain.SPNeedCtlColor then begin
        //Result:= DefWindowProc(h, msg, wParam, lParam);
        DeleteObject(FI^.lpCtlBrush);
        DC:= CreateCompatibleDC(FI^.lpSurface);
        GetClientRect(lParam, R);
        FillBmiInfo(r.Right, r.Bottom, @bmiInfo);
        mBmp:= CreateDibSection(DC, bmiInfo, DIB_RGB_COLORS, mBits, 0, 0);
        oBmp:= SelectObject(DC, mBmp);
        SetBkMode(wParam, 1);
        GetWindowRect(lParam, r1);
        MapWindowPoints(0, h, r1, 2);
        BitBlt(DC, 0, 0, r.Right, r.Bottom, FI^.lpSurface, r1.Left, r1.Top, SRCCOPY);
        if FI^.lpIsLayered then begin
          i:= r.Bottom*R.Right;
          rgb:= mBits;
          if i<>0 then
          repeat
            Dec(i);
            if rgb^.rgbReserved=0 then
              rgb^.rgbReserved:= 1;
            Inc(rgb);
          until i=0;
        end;

        FI^.lpCtlBrush:= CreatePatternBrush(mBmp);
        SelectObject(DC, oBmp);
        DeleteObject(DC);
        DeleteObject(mBmp);

        Result:= FI^.lpCtlBrush;
      end;
    end;
    WM_SIZE:  begin
      Result:= 0;

      r.Right:= Word(lParam);
      r.Bottom:= lParam shr 16;

      if (FI^.lpWidth<>r.Right)or(FI^.lpHeight<>r.Bottom) then begin
        FillBmiInfo(r.Right, r.Bottom, @bmiInfo);
        FI^.lpBitsCount:= r.Right*r.Bottom;

        GdipDeleteGraphics(FI^.lpSurfaceGP);
        SelectObject(FI^.lpSurface, FI^.lpSurfaceOld);
        DeleteObject(FI^.lpSurfaceBmp);
        FI^.lpSurfaceBmp    := CreateDibSection(FI^.lpSurface, bmiInfo, DIB_RGB_COLORS, FI^.lpSurfaceBits, 0, 0);
        FI^.lpSurfaceOld    := SelectObject(FI^.lpSurface, FI^.lpSurfaceBmp);
        GdipCreateFromHDC(FI^.lpSurface, FI^.lpSurfaceGP);

        GdipDeleteGraphics(FI^.lpLayerGP);
        SelectObject(FI^.lpLayer, FI^.lpLayerOld);
        DeleteObject(FI^.lpLayerBmp);
        FI^.lpLayerBmp      := CreateDibSection(FI^.lpLayer, bmiInfo, DIB_RGB_COLORS, FI^.lpLayerBits, 0, 0);
        FI^.lpLayerOld      := SelectObject(FI^.lpLayer, FI^.lpLayerBmp);
        GdipCreateFromHDC(FI^.lpLayer, FI^.lpLayerGP);

        GdipDeleteGraphics(FI^.lpBkLayerGP);
        SelectObject(FI^.lpBkLayer, FI^.lpBkLayerOld);
        DeleteObject(FI^.lpBkLayerBmp);
        FI^.lpBkLayerBmp    := CreateDibSection(FI^.lpBkLayer, bmiInfo, DIB_RGB_COLORS, FI^.lpBkLayerBits, 0, 0);
        FI^.lpBkLayerOld    := SelectObject(FI^.lpBkLayer, FI^.lpBkLayerBmp);
        GdipCreateFromHDC(FI^.lpBkLayer, FI^.lpBkLayerGP);
      end;
    end;
    WM_PAINT: begin
      Result:= 0;
      hMemDC:= BeginPaint(h, ps);
      if wParam <> 0 then DC:= HDC(wParam) else DC:= hMemDC;
			
      CallWindowProc(Pointer(FI^.lpOldProc), h, WM_ERASEBKGND, Longint(FI^.lpSurface), 0);
      BlendBits(FI^.lpBkLayerBits, FI^.lpSurfaceBits, FI^.lpBitsCount);

      i:= MAX_SPRITES-1;
      repeat
        SP:= PSprite(Longword(FI^.lpSprites) + Longword(i*SizeOf(TSprite)));
        if (SP^.spFlags and SP_GIFDRAW) <> 0 then
          if SP^.spShowImg then spDrawImage(FI^.lpSurfaceGP, SP); {my_add}
        Dec(i);
      until i < FI^.lpSpritesCount;

      CallWindowProc(Pointer(FI^.lpOldProc), h, WM_PAINT, Longint(FI^.lpSurface), 0);
      spPaintShadowText(h, FI^.lpSurfaceGP);
      BlendBits(FI^.lpLayerBits, FI^.lpSurfaceBits, FI^.lpBitsCount);
			
      BitBlt(DC, ps.rcPaint.Left, ps.rcPaint.Top, ps.rcPaint.Right-ps.rcPaint.Left,ps.rcPaint.Bottom-ps.rcPaint.Top, FI^.lpSurface, ps.rcPaint.Left ,ps.rcPaint.Top, SRCCOPY);
      EndPaint(h, ps);
		end;
    WM_TIMER: begin
      if wParam=SP_PAINTTIMER then begin
        Result:= 0;
        spUpdateLayeredWindow(h, FI);
      end else
        Result:= CallWindowProc(Pointer(FI^.lpOldProc), h, msg, wParam, lParam);
    end;
  else
    Result:= CallWindowProc(Pointer(FI^.lpOldProc), h, msg, wParam, lParam);
  end;
end;

function spAddFormInfo(hWnd: HWND): PFormInfo;
var
  FI: PFormInfo;
  DC: HDC;
  r: TRect;
  bmiInfo: TBitmapInfo;
  sc, tc, bc, ts: Longword;
begin
  Result:= nil;
  if (ISSpriteMain.InfoCount = 0)or(not ISSpriteMain.ISSpriteInit) then Exit;

  Dec(ISSpriteMain.InfoCount);
  FI:= PFormInfo(Longword(ISSpriteMain.InfoPoint) + Longword(ISSpriteMain.InfoCount*SizeOf(TFormInfo)));
  FillChar(FI^, SizeOf(TFormInfo), 0);
  with FI^ do begin
    lpHandle        := hWnd;
    lplwaAlpha      := 255;
    lpOldProc       := SetWindowLong(hWnd, GWL_WNDPROC, Longint(@spWndPaint));
    lpIsChild       := (GetWindowLong(hWnd, GWL_STYLE) and WS_CHILD) <> 0;
    lpBlendOp       := $01FF0000;

    sc:= SizeOf(TSprite)*MAX_SPRITES;
    tc:= SizeOf(TShadowText)*MAX_TEXTS;
    bc:= SizeOf(TImgButton)*MAX_BUTTONS;
    ts:= sc+tc+bc;

    lpRawData       := VirtualAlloc(nil, ts, MEM_RESERVE or MEM_COMMIT, PAGE_READWRITE);
    lpSprites       := lpRawData;
    lpSpritesCount  := MAX_SPRITES;
    lpSpritesBkCount:= 0;
    lpSpritesFrCount:= 0;
    lpTexts         := Pointer(Longword(lpSprites)+sc);
    lpTextsCount    := MAX_TEXTS;
    lpButtons       := Pointer(Longword(lpTexts)+tc);
    lpButtonsCount  := MAX_BUTTONS;
    lpTimer         := INVALID_HANDLE_VALUE;

    GetClientRect(hWnd, r);
    FillBmiInfo(r.Right, r.Bottom, @bmiInfo);
    lpWidth         := r.Right;
    lpHeight        := r.Bottom;

    DC              := GetDC(hWnd);

    lpSurface       := CreateCompatibleDC(DC);
    lpSurfaceBmp    := CreateDibSection(DC, bmiInfo, DIB_RGB_COLORS, lpSurfaceBits, 0, 0);
    lpSurfaceOld    := SelectObject(lpSurface, lpSurfaceBmp);
    GdipCreateFromHDC(lpSurface, lpSurfaceGP);

    lpLayer         := CreateCompatibleDC(DC);
    lpLayerBmp      := CreateDibSection(DC, bmiInfo, DIB_RGB_COLORS, lpLayerBits, 0, 0);
    lpLayerOld      := SelectObject(lpLayer, lpLayerBmp);
    GdipCreateFromHDC(lpLayer, lpLayerGP);

    lpBkLayer       := CreateCompatibleDC(DC);
    lpBkLayerBmp    := CreateDibSection(DC, bmiInfo, DIB_RGB_COLORS, lpBkLayerBits, 0, 0);
    lpBkLayerOld    := SelectObject(lpBkLayer, lpBkLayerBmp);
    GdipCreateFromHDC(lpBkLayer, lpBkLayerGP);

    lpBitsCount     := (r.Right*r.Bottom);

    ClearBits(lpBkLayerBits, lpBitsCount);
    ClearBits(lpLayerBits, lpBitsCount);

    ReleaseDC(hWnd, DC);
  end;
  Result:= FI;
end;