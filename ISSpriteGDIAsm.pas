{
procedure ClearBits(srcBits: PDWORD; Count: Integer);
asm
    push  edi

    mov	  ecx, edx
    mov   edi, eax
    xor   eax, eax

    rep   stosd

    pop   edi
end;
}
procedure ClearBits(srcBits: Pointer; Count: Integer);
asm
  push  ebx
  push  edi

  xor   ebx, ebx
  movd  mm0, ebx

  mov   ecx, edx
  mov   edx, eax
  shl   ecx, 02h
  and   edx, 07h
  sub   edx, 07h
  neg   edx

  lea   edi, @altable
  jmp   [edi+edx*4]

@altable:   dd offset @al1
            dd offset @al2
            dd offset @al3
            dd offset @al4
            dd offset @al5
            dd offset @al6
            dd offset @al7
            dd offset @start

@al7: mov [eax], bl
      inc eax
@al6: mov [eax], bl
      inc eax
@al5: mov [eax], bl
      inc eax
@al4: mov [eax], bl
      inc eax
@al3: mov [eax], bl
      inc eax
@al2: mov [eax], bl
      inc eax
@al1: mov [eax], bl
      inc eax

@start:
  inc   edx
  lea   edi, @filltable
  sub   ecx, edx
  mov   edx, ecx
  shr   ecx, 03h
  and   edx, 07h
  jmp   [edi+edx*4]

@filltable:   dd offset @faln
              dd offset @fl1
              dd offset @fl2
              dd offset @fl3
              dd offset @fl4
              dd offset @fl5
              dd offset @fl6
              dd offset @fl7

@fl7: mov [eax+ecx*8+06h], bl
@fl6: mov [eax+ecx*8+05h], bl
@fl5: mov [eax+ecx*8+04h], bl
@fl4: mov [eax+ecx*8+03h], bl
@fl3: mov [eax+ecx*8+02h], bl
@fl2: mov [eax+ecx*8+01h], bl
@fl1: mov [eax+ecx*8], bl

@faln:
  dec   ecx
  movq  [eax+ecx*8], mm0
  jz  @exit
  jmp @faln

@exit:
  emms

  pop edi
  pop ebx
end;
{
function RoundSqrt(x1, x2: Integer): Integer;
asm
  imul  edx, edx
  sub   eax, edx

  add   esp, 00FFFFFFF8h
  mov   [esp], eax

  fild  dword ptr [edx]
  fsqrt
  fistp qword ptr [edx]
  fwait

  pop   eax
  pop   edx
end;
}
                    //eax             //edx             //ecx
procedure BlendBits(SrcBits: Pointer; DstBits: Pointer; Count: Integer);
asm
  push      ebx
  push      edi
  push      esi

  mov       edi, eax  // pSrc
  mov       esi, edx  // pDst
  shl       ecx, 02h  // *4

  xor       edx, edx
  movd      mm4, edx

@loop:
  sub       ecx, 04h

	mov		    al, [edi+ecx+03h]
	test	    al, al
	jz		    @next

  mov       ebx, [esi+ecx]
  test      ebx, ebx
  jnz       @premul
  mov       ebx, [edi+ecx]
  mov       [esi+ecx], ebx
  jmp       @next

@premul:
  mov       ah, al
  mov       bx, ax
  shl       eax, 0010h
  or        ax, bx

  movd      mm3, eax
  punpcklbw mm3, mm4

  movd      mm1, [edi+ecx]
  punpcklbw mm1, mm4

  movd      mm0, [esi+ecx]
  punpcklbw mm0, mm4

  movq      mm2, mm0
  psllw     mm0, 08h
  pmullw    mm2, mm3
  psubw     mm0, mm2
  psrlw     mm0, 08h

  paddw     mm0, mm1
  packuswb  mm0, mm0

  movd      [esi+ecx], mm0

@next:
  test      ecx, ecx
  jnz       @loop

  emms

  pop       esi
  pop       edi
  pop       ebx
end;
                      //eax             //edx             //ecx           //ebp+08h
procedure BlendBitsII(SrcBits: Pointer; DstBits: Pointer; Count: Integer; Alpha: Byte);
asm
	push 	    ebx
	push	    edi
	push	    esi

	mov		    edi, eax        // pSrc
	mov		    esi, edx        // pDst
  shl       ecx, 02h        // *4

  xor       eax, eax        // eax = 0
  movd      mm4, eax        // mm4 = 0

	mov		    bl, [ebp+08h]   // bl = Alpha
	test	    bl, bl
	jz		    @exit

  mov       bh, bl
  mov       ax, bx
  shl       ebx, 10h        // ebx = Alpha : Alpha : Alpha : Alpha
  or        bx, ax

  movd      mm3, ebx
  punpcklbw mm3, mm4        // mm3 = Alpha

@loop:
  sub       ecx, 04h        // c =- 4

  mov       edx, [edi+ecx]
  test      edx, edx        // if ([pSrc + count] == 0) goto next
  jz        @next

  movd      mm2, edx        // mm1 = [pSrc + count]
  punpcklbw mm2, mm4

  pmullw    mm2, mm3        // == a4*b4 : a3*b3 : a2*b2 : a1*b1
  psrlw     mm2, 08h        // == a4 >> 8 : a3 >> 8 : a2 >> 8 : a1 >> 8

  movq      mm0, mm2        //
  packuswb  mm0, mm0        // == a4 > 255 ? 255 : a4 : ...

  movd      eax, mm0        // eax = src*(Alpha/255)
  shr       eax, 018h       // eax >> 24
  test      al, al          // al = NewAlpha
  jz        @next

	mov		    edx, [esi+ecx]  // edx = [pDst+count]
	test	    edx, edx
	jnz		    @premulte       // if edx != 0
  movd      [esi+ecx], mm0
	jmp       @next
  
@premulte:
  mov       ah, al
  mov       dx, ax
  shl       eax, 10h
  or        ax, dx

  movd      mm1, eax
  punpcklbw mm1, mm4

  movd      mm0, [esi+ecx]  // mm0 = [pDst + count]
  punpcklbw mm0, mm4

  movq      mm5, mm0
  pmullw    mm5, mm1
  psllw     mm0, 08h
  psubw     mm0, mm5
  psrlw     mm0, 08h

  paddw     mm0, mm2
  packuswb  mm0, mm0

  movd      [esi+ecx], mm0  // [pDst + count] = mm0

@next:
	test	    ecx, ecx
	jnz		    @loop
	
@exit:
  emms

	pop		    esi
	pop		    edi
	pop		    ebx
end;
{
procedure MaskBits(srcBits, dstBits: Pointer; Color: DWORD; Count: Integer);
asm
	push 	    ebx
	push 	    esi
	push 	    edi

	mov		    edi, eax
	mov		    esi, edx
  xchg      ecx, [ebp+08h]
  shl       ecx, 02h
  xor       ebx, ebx

  movd      mm3, ebx
  mov       ebx, 00FFFFFFh
  movd      mm2, [ebp+08h]
  punpcklbw mm2, mm3

@loop:
  sub       ecx, 04h

	mov		    edx, [edi+ecx]
	and		    edx, ebx
	jz		    @next
  shr       edx, 0010h
	jz		    @next

	mov		    dh, dl
  mov       ax, dx
  shl       edx, 010h
  or        dx, ax

  movd      mm1, edx
  punpcklbw mm1, mm3

  movq      mm0, mm2

  pmullw    mm0, mm1
  psrlw     mm0, 08h
  packuswb  mm0, mm0

  movd      [esi+ecx], mm0

  mov		    [esi+ecx+03h], al

@next:
	test	    ecx, ecx
	jnz		    @loop

  emms

	pop     	edi
	pop		    esi
	pop		    ebx
end;
}                  //eax    //edx             //ecx         //ebp+08h
procedure MaskBits(srcBits, dstBits: Pointer; Color: DWORD; Count: Integer);
asm
	push 	    ebx
	push 	    esi
	push 	    edi

	mov		    edi, eax
	mov		    esi, edx
  xchg      ecx, [ebp+08h]
  xor       ebx, ebx

  movd      mm3, ebx
  mov       ebx, 00FFFFFFh
  movd      mm2, [ebp+08h]
  punpcklbw mm2, mm3

@loop:
  dec       ecx

	mov		    edx, [edi+ecx*4]
	and		    edx, ebx
	jz		    @next
  shr       edx, 0010h
	jz		    @next

  mov       eax, edx
  shl       eax, 08h
  or        edx, eax
  mov       eax, edx
  shl       eax, 010h
  or        edx, eax

  movd      mm1, edx
  punpcklbw mm1, mm3

  movq      mm0, mm2

  pmullw    mm0, mm1
  psrlw     mm0, 08h
  packuswb  mm0, mm0

  movd      [esi+ecx*4], mm0

  mov		    [esi+ecx*4+03h], dl

@next:
	test	    ecx, ecx
	jnz		    @loop

  emms

	pop     	edi
	pop		    esi
	pop		    ebx
end;

procedure CalcPixel(Pix: Pointer; Dest: Pointer);
asm
  movq      mm0, [edx]
  movd      mm1, [eax]
  xor       eax, eax
  movd      mm2, eax
  punpcklbw mm1, mm2
  paddw     mm0, mm1
  movq      [edx], mm0
end;

procedure SetRectI(Rect: PRect; Vals: Pointer);
asm
//push  ecx

  mov   ecx, [edx]
  mov   [eax], ecx
  add   ecx, [edx+08h]
  mov   [eax+08h], ecx

  mov   ecx, [edx+04h]
  mov   [eax+04h], ecx
  add   ecx, [edx+0Ch]
  mov   [eax+0Ch], ecx

//pop   ecx
end;

procedure SetRectII(Rect: PRect; Vals: Pointer);
asm
  movq  mm0, [edx]
  movq  [eax], mm0
  movq  mm0, [edx+08h]
  movq  [eax+08h], mm0
  emms
end;

procedure SetRectIII(Rect: PRect; Width, Height: Integer);
asm
  push  ebx

  xor   ebx, ebx
  mov   [eax], ebx
  mov   [eax+04h], ebx
  mov   [eax+08h], edx
  mov   [eax+0Ch], ecx

  pop   ebx
end;

procedure FillShadow(ppxs: PPixelStruct0);
asm
  push  ebx
  push  esi
  push  edi
  push  ebp

  sub   esp, 000Ch
  mov   edi, eax
//mov   [esp+00Ch], 00FFh

finit

fldpi
mov     [esp], 04h
fild    dword ptr [esp]
fdivp   st(1), st(0)
fsin 							   						//sin(PI/4);

mov     ebp, [edi+002Ch] 				//dr:= ppxs^.radius
mov     ecx, [edi+020h]
//ebp = dr

@loop1:

	mov     [esp], ebp        		// [esp] = dr
	fild    dword ptr [esp]  			// ST(0) = dr = [esp]
	fmul    st(0), st(1)     			// ST(0) = ST(0)*ST(1) = dr*sin(PI/4);
//frndint               				// ST(0) = round(ST(0))
	fistp   dword ptr [esp+04h]  	// [esp+04h] = mx:= round(dr*sin(PI/4))
	fwait

	//[esp+04h] = mx

	mov     eax, ebp
	mul     eax
	mov     [esp+08h], eax    // [esp+08h] = r2:= dr*dr
	
	mov     esi, [esp+04h]
	neg	    esi               // esi = dx:= -mx

	xor     eax, eax
	xor     edx, edx

	mov     eax, [edi+0030h]
	shl     eax, 03h
	add     eax, 00FFh
	div     ebp
  cmp     eax, 00FFh
  jle     @premul
  mov     eax, 00FFh

  @premul:
	  test 	al, al          		// if rgbA=0 then next
	  jz 		@next

	  mov 	bl, al
	  mov   [edi+0027h], al 	// ppxs.rgbA:= rgbA
	//xor   edx, edx
	  xor   eax, eax

	  mov 	al, [edi+0034h]
	  mul   bl
	//shr   ax, 08h
	//div   byte ptr [esp+000Ch]	//00FFh
	  mov   [edi+0024h], ah 			//ppxs.rgbR

	  mov 	al, [edi+0035h]
  	mul   bl
	//shr   ax, 08h
	//div   byte ptr [esp+000Ch]	//00FFh
	  mov   [edi+0025h], ah 			//ppxs.rgbG

	  mov 	al, [edi+0036h]
	  mul   bl
	//shr   al, 08h
	//div   byte ptr [esp+000Ch]	//00FFh
	  mov   [edi+0026h], ah 			//ppxs.rgbB

    mov   ebx, [edi+024h]
	//mov   ecx, [edi+020h]
    mov   dl, [edi+027h]

	  @loop2:
		  mov   eax, esi
		  imul  eax, esi
		  mov  [esp], eax      			// dx*dx
		  fild  dword ptr [esp+08h]
		  fild  dword ptr[esp]
		  fsubp st(1), st(0)   			// r2-(dx*dx)
		  fsqrt
		  fwait
		  fistp dword ptr [esp]     //dy:= sqrt(r2-(dx*dx))

    @next0:
      mov   eax, [esp]
      imul  eax, [edi+010h]
      add   eax, esi
      shl   eax, 02h
      add   eax, ecx
      cmp   eax, [edi+01Ch]
      jge   @next1
      cmp   eax, [edi+018h]
      jl    @next1
		//mov   dl, [eax+03h]
		//cmp   dl, [edi+027h]
      cmp   [eax+03h], dl
      jnc   @next1
      mov   [eax], ebx       			//SetByte(ppxs, dx, dy)

    @next1:
      mov   eax, [esp]
      neg   eax
      imul  eax, [edi+010h]
      add   eax, esi
      shl   eax, 02h
      add   eax, ecx
      cmp   eax, [edi+01Ch]
      jge   @next2
      cmp   eax, [edi+018h]
      jl    @next2
    //mov   dl, [eax+03h]
    //cmp   dl, [edi+027h]
      cmp   [eax+03h], dl
      jnc   @next2
      mov   [eax], ebx         		//SetByte(ppxs, dx, -dy)

    @next2:
      mov   eax, esi
      imul  eax, [edi+010h]
      add   eax, [esp]
      shl   eax, 02h
      add   eax, ecx
      cmp   eax, [edi+01Ch]
      jge   @next3
      cmp   eax, [edi+018h]
      jl    @next3
    //mov   dl, [eax+03h]
    //cmp   dl, [edi+027h]
      cmp   [eax+03h], dl
      jnc   @next3
      mov   [eax], ebx         		//SetByte(ppxs, dy, dx)

    @next3:
      mov   eax, esi
      imul  eax, [edi+010h]
      sub   eax, [esp]
      shl   eax, 02h
      add   eax, ecx
      cmp   eax, [edi+01Ch]
      jge   @next4
      cmp   eax, [edi+018h]
      jl   @next4
    //mov   dl, [eax+03h]
    //cmp   dl, [edi+027h]
      cmp   [eax+03h], dl
      jnc   @next4
      mov   [eax], ebx        		//SetByte(ppxs, -dy, dx)

    @next4:
		  inc esi
		  cmp esi, [esp+04h] 				 	// if dx>mx then next
		  jle @loop2

  @next:
	  dec ebp
	//test ebp, ebp
    jnz  @loop1           				// if dr=0 then exit

  add esp, 000Ch

  pop ebp
  pop edi
  pop esi
  pop ebx
end;